package com.mariocorp.mariotwitter.retrofit.auth;

import com.mariocorp.mariotwitter.common.Const;
import com.mariocorp.mariotwitter.retrofit.AuthInterceptor;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MiniTwitterClientAuth {
    private static MiniTwitterClientAuth instance = null;
    private MiniTwitterServiceAuth miniTwitterServiceAuth;
    private Retrofit retrofit;

    public MiniTwitterClientAuth() {
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();

        //INTERCEPTOR PARA AGREGAR EL TOKEN EN HEADER
        httpClient.addInterceptor(new AuthInterceptor());

        //INTERCEPTOR PARA MOSTRAR LOGS DE LAS PETICIONES
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        httpClient.addInterceptor(logging);// REMEMBER: add logging as last interceptor

        retrofit = new Retrofit.Builder()
                .baseUrl(Const.API_MINI_TWITTER_BASE)
                .addConverterFactory(GsonConverterFactory.create())
                .client(httpClient.build())
                .build();
        miniTwitterServiceAuth = retrofit.create(MiniTwitterServiceAuth.class);
    }

    public static MiniTwitterClientAuth getInstance() {
        if(instance == null) { instance = new MiniTwitterClientAuth(); }
        return instance;
    }

    public MiniTwitterServiceAuth getMiniTwitterServiceAuth() {
        return miniTwitterServiceAuth;
    }

}
