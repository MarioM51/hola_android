package com.mariocorp.mariotwitter.ui.fragment;

import androidx.lifecycle.ViewModelProviders;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.google.android.material.navigation.NavigationView;
import com.mariocorp.mariotwitter.R;
import com.mariocorp.mariotwitter.common.Const;
import com.mariocorp.mariotwitter.data.viewModel.TweetViewModel;

public class BottonModalTweetFragment extends BottomSheetDialogFragment {
    private TweetViewModel tweetViewModel;
    private int idTweetDelete;

    public static BottonModalTweetFragment newInstance(int idTweet) {
        BottonModalTweetFragment bottonModalTweetFragment = new BottonModalTweetFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(Const.ARG_TWEET_ID, idTweet);
        /* CUIDADO */ bottonModalTweetFragment.setArguments(bundle);
        return bottonModalTweetFragment;
    }


    @Override public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(getArguments() != null) {
            idTweetDelete = getArguments().getInt(Const.ARG_TWEET_ID);
        }
    }

    @Override public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.botton_modal_tweet_fragment, container, false);
        final NavigationView navigationView =  v.findViewById(R.id.navigation_view_bottom_tweet);
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {

            @Override public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                int idOptionSelected = menuItem.getItemId();
                if(idOptionSelected == R.id.action_delete_tweet) {
                    tweetViewModel.deleteTweet(idTweetDelete);
                    getDialog().dismiss();
                    return true;
                }
                return false;
            }
        });
        return v;
    }

    @Override public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        tweetViewModel = ViewModelProviders.of(getActivity()).get(TweetViewModel.class);
    }
}
