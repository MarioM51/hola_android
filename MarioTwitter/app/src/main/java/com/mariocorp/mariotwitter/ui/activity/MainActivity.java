package com.mariocorp.mariotwitter.ui.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.mariocorp.mariotwitter.R;
import com.mariocorp.mariotwitter.common.Const;
import com.mariocorp.mariotwitter.common.SharedPreferencesManager;
import com.mariocorp.mariotwitter.data.dto.request.LoginRequest;
import com.mariocorp.mariotwitter.data.dto.response.LoginResponse;
import com.mariocorp.mariotwitter.retrofit.MiniTwitterClient;
import com.mariocorp.mariotwitter.retrofit.MiniTwitterService;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String TAG = "MainActivity";
    //ui
    private TextView btToSingup;
    private EditText editTextEmail, editTextPassword;
    private Button btnDoLogin;

    //api
    private MiniTwitterClient miniTwitterClient;
    private MiniTwitterService miniTwitterService;


    private void setuptUI() {
        btToSingup = findViewById(R.id.btn_to_registrar);
        btToSingup.setOnClickListener(this);

        editTextEmail = findViewById(R.id.editTextEmail);

        editTextPassword = findViewById(R.id.editTextPassword);

        btnDoLogin = findViewById(R.id.btnLogin);
        btnDoLogin.setOnClickListener(this);
    }

    private void retrofitInit() {
        miniTwitterClient = MiniTwitterClient.getInstance();
        miniTwitterService = miniTwitterClient.getMiniTwitterService();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_main);
        this.retrofitInit();
        setuptUI();
    }




    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_to_registrar:
                Intent i = new Intent(MainActivity.this, SingupActivity.class);
                startActivity(i);
            break;

            case R.id.btnLogin:
                this.doLogin();
                break;
        }
    }

    private void doLogin() {
        String inputEmail =  editTextEmail.getText().toString();
        String inputPass =  editTextPassword.getText().toString();
        Log.i(TAG, String.format("email: %s | pass: %s", inputEmail, inputPass));

        if(inputEmail.isEmpty()) { editTextEmail.setError("El email es requerido");       return ; }
        if(inputPass.isEmpty()) {  editTextPassword.setError("El password es requerido"); return ; }

        LoginRequest loginRequest = new LoginRequest(inputEmail, inputPass);
        Call<LoginResponse> call = miniTwitterService.doLogin(loginRequest);
        call.enqueue(new Callback<LoginResponse>() {
            @Override public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                if(response.isSuccessful()) {//esta en el rango del 200
                    //GUARDAMOS DATOS DE USUARIO EN SHAREDPREFERENCES
                    SharedPreferencesManager.saveString(Const.TOKEN, response.body().getToken());
                    SharedPreferencesManager.saveUserData(response.body().getUsername(),
                            response.body().getEmail(), response.body().getPhotoUrl(),
                            response.body().getCreated(), response.body().getActive(), -1);

                    //NOS PASAMOS A DASHBOARD
                    Intent i = new Intent(MainActivity.this, DashnoardActivity.class);
                    startActivity(i);
                    finish(); // lo terminamos ya que el usuario no requerira volver al login
                } else {
                    Toast.makeText(MainActivity.this, "Reviza los datos", Toast.LENGTH_LONG).show();
                }
            }

            @Override public void onFailure(Call<LoginResponse> call, Throwable t) {
                Toast.makeText(MainActivity.this, "Problemas de conexion", Toast.LENGTH_LONG).show();
            }
        });

    }
}
