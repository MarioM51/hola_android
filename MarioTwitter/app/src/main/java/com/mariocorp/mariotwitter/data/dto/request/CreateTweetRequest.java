package com.mariocorp.mariotwitter.data.dto.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CreateTweetRequest {
    @SerializedName("mensaje") @Expose private String mensaje;

    public CreateTweetRequest() { }
    public CreateTweetRequest(String mensaje) {
        super();
        this.mensaje = mensaje;
    }
    public String getMensaje() { return mensaje; }
    public void setMensaje(String mensaje) { this.mensaje = mensaje; }
}
