package com.mariocorp.mariotwitter.ui.activity;

import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.single.PermissionListener;
import com.mariocorp.mariotwitter.R;
import com.mariocorp.mariotwitter.common.Const;
import com.mariocorp.mariotwitter.common.MyApp;
import com.mariocorp.mariotwitter.common.SharedPreferencesManager;
import com.mariocorp.mariotwitter.data.viewModel.ProfileViewModel;
import com.mariocorp.mariotwitter.ui.fragment.ProfileFragment;
import com.mariocorp.mariotwitter.ui.fragment.TweetsFragment;
import com.mariocorp.mariotwitter.ui.fragment.AddTweetFormDialogFragment;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

public class DashnoardActivity extends AppCompatActivity implements PermissionListener {
    private static final String TAG = "DashnoardActivity";

    private FloatingActionButton btnToAddTweet;
    private BottomNavigationView navView;
    private ImageView ivAvatar;
    ProfileViewModel profileViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_dashnoard);

        profileViewModel = ViewModelProviders.of(this).get(ProfileViewModel.class);

        navView = findViewById(R.id.nav_view);
        navView.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        //AGREGAMOS EVENTO DEL MENU DE NAVEGACION INFERIOR
        btnToAddTweet = findViewById(R.id.btn_to_add_tweet);
        btnToAddTweet.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                AddTweetFormDialogFragment dialog = new AddTweetFormDialogFragment();
                dialog.show(getSupportFragmentManager(), "AddTweetFormDialogFragment");
            }
        });
        getSupportFragmentManager().beginTransaction()
                .add(R.id.fragment_container, TweetsFragment.newInstance(Const.TWEET_LIST_ALL))
                .commit();

        ivAvatar = findViewById(R.id.dashboard_perfil_photo);
        String photoUrl = SharedPreferencesManager.getsomeString(Const.USER_PHOTOURL);
        if(!photoUrl.isEmpty()) {
            Glide.with(this)
                    .load(Const.API_GET_PHOTO_USER + photoUrl)
                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                    .centerCrop()
                    .skipMemoryCache(true)
                    .into(ivAvatar);
        }

        profileViewModel.getUploadPhotoUserprofile().observe(this, new Observer<String>() {
            @Override
            public void onChanged(String photoName) {
                if(photoName.isEmpty()) { return ;}
                Glide.with(MyApp.getContext()).load(Const.API_GET_PHOTO_USER+photoName)
                        .dontAnimate()
                        .diskCacheStrategy(DiskCacheStrategy.NONE)
                        .skipMemoryCache(true)
                        .into(ivAvatar);
            }
        });
    }

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener = new BottomNavigationView.OnNavigationItemSelectedListener() {
        @Override public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            Log.i(TAG, "Entro evento mOnNavigationItemSelectedListener");
            Fragment fragment = null;
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    fragment = TweetsFragment.newInstance(Const.TWEET_LIST_ALL);
                    btnToAddTweet.show();
                break;
                case R.id.navigation_favs:
                    fragment = TweetsFragment.newInstance(Const.TWEET_LIST_FAVS);
                    btnToAddTweet.hide();
                break;
                case R.id.navigation_account:
                    fragment = ProfileFragment.newInstance();
                    btnToAddTweet.hide();
                break;
                default: Log.e(TAG, "Algo a hido mal en el switch"); break;
            }
            if(fragment != null) {
                getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.fragment_container, fragment)
                        .commit();
                return true;
            }
            Log.e(TAG, "Algo a ido mal creando el fragment de dashboard");
            return false;
        }
    };

    @Override
    public void onPermissionGranted(PermissionGrantedResponse response) {
        Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(intent, Const.SELECT_PHOTO);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode != RESULT_CANCELED) {
            if (requestCode == Const.SELECT_PHOTO) {
                if (data != null) {
                    Uri imgSelected = data.getData();
                    String[] partsPath = {MediaStore.Images.Media.DATA};
                    Cursor cursor = getContentResolver().query(imgSelected, partsPath, null, null, null);
                    if (cursor != null) {
                        cursor.moveToFirst();
                        int imageIndex = cursor.getColumnIndex(partsPath[0]);
                        String photopath = cursor.getString(imageIndex);
                        Log.i(TAG, "Archivo a subir: " + photopath);
                        profileViewModel.uploadPhoto(photopath);
                        cursor.close();
                    }

                }
            }
        }
    }

    @Override
    public void onPermissionDenied(PermissionDeniedResponse response) {
        String msj = "No se puede seleccionar la foto por falta de permisos";
        Toast.makeText(MyApp.getContext(), msj, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onPermissionRationaleShouldBeShown(PermissionRequest permission, PermissionToken token) {

    }
}
