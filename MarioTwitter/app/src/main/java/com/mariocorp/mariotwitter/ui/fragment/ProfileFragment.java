package com.mariocorp.mariotwitter.ui.fragment;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import android.Manifest;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.listener.single.CompositePermissionListener;
import com.karumi.dexter.listener.single.DialogOnDeniedPermissionListener;
import com.karumi.dexter.listener.single.PermissionListener;
import com.mariocorp.mariotwitter.R;
import com.mariocorp.mariotwitter.common.Const;
import com.mariocorp.mariotwitter.common.MyApp;
import com.mariocorp.mariotwitter.data.dto.response.ChangeProfileRequest;
import com.mariocorp.mariotwitter.data.dto.response.GetProfileResponse;
import com.mariocorp.mariotwitter.data.viewModel.ProfileViewModel;

public class ProfileFragment extends Fragment {

    private ProfileViewModel profileViewModel;

    private ImageView imgPhotoProfile;
    private EditText inUsername, inEmil, inCueerentPasword, inWebsite, inDescription;
    private Button btnSaveProfile, btnChangePassword;
    private boolean updateProfiledata = false;
    private PermissionListener permissionListener;

    public static ProfileFragment newInstance() {
        return new ProfileFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // CUIDADO profileViewModel = ViewModelProviders.of(this).get(ProfileViewModel.class);
        profileViewModel = ViewModelProviders.of(getActivity()).get(ProfileViewModel.class);

    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.profile_fragment, container, false);
        //images
        imgPhotoProfile = v.findViewById(R.id.img_profile_photo);
        imgPhotoProfile.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                checkPermission();
            }
        });

        //editText
        inUsername = v.findViewById(R.id.inttxt_username);
        inEmil = v.findViewById(R.id.intxt_profile_email);
        inWebsite = v.findViewById(R.id.intext_wetsite);
        inDescription = v.findViewById(R.id.intext_profile_description);
        inCueerentPasword = v.findViewById(R.id.intext_current_pass);

        //btn
        btnSaveProfile = v.findViewById(R.id.btn_save_profile);
        btnSaveProfile.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                updateProfiledata = true;
                //capturamos info
                String username = inUsername.getText().toString();
                String email = inEmil.getText().toString();
                String website = inWebsite.getText().toString();
                String description = inDescription.getText().toString();
                String pass = inCueerentPasword.getText().toString();

                //hacemos chequeo
                boolean valueRequiredNotFound = false;
                if(username.isEmpty()) {
                    inUsername.setError(getString(R.string.input_required)); valueRequiredNotFound = true;
                }
                if(email.isEmpty()) {
                    inEmil.setError(getString(R.string.input_required)); valueRequiredNotFound = true;
                }
                if(pass.isEmpty()) {
                    inCueerentPasword.setError(getString(R.string.input_required)); valueRequiredNotFound = true;
                }
                if(valueRequiredNotFound) { return ; }
                disableButton(btnSaveProfile);


                //preparamos peticion
                ChangeProfileRequest changeProfileRequest = new ChangeProfileRequest();
                changeProfileRequest.setUsername(username);
                changeProfileRequest.setEmail(email);
                changeProfileRequest.setWebsite(website);
                changeProfileRequest.setDescripcion(description);
                changeProfileRequest.setPasword(pass); // DESACTIVAR PARA PROBAR LA CAPTURA DE ERRORES DE LA API


                //hacemos peticion, se va actualizar ya que tenemos un observer ya implementado
                profileViewModel.changeUserprofile(changeProfileRequest);

            }
        });

        btnChangePassword = v.findViewById(R.id.btn_change_pass);
        btnChangePassword.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                String log = "Cambiar contraseña iniciado";
                Toast.makeText(getActivity(), log, Toast.LENGTH_LONG).show();
            }
        });

        profileViewModel.getUserprofile().observe(getActivity(), new Observer<GetProfileResponse>() {
            @Override public void onChanged(GetProfileResponse getProfileResponse) {
                inUsername.setText(getProfileResponse.getUsername());
                inEmil.setText(getProfileResponse.getEmail());
                inWebsite.setText(getProfileResponse.getWebsite());
                inDescription.setText(getProfileResponse.getDescripcion());
                if(!getProfileResponse.getPhotoUrl().isEmpty()) {
                    Glide.with(MyApp.getContext()).load(Const.API_GET_PHOTO_USER+getProfileResponse.getPhotoUrl())
                            .dontAnimate()
                            .diskCacheStrategy(DiskCacheStrategy.NONE)
                            .skipMemoryCache(true)
                            .into(imgPhotoProfile);
                }

                if(updateProfiledata) {
                    Toast.makeText(MyApp.getContext(), "Datos guardados", Toast.LENGTH_LONG).show();
                }
                enableButton(btnSaveProfile);
            }
        });


        //OBSERVAMOS CUALQUIER ERROR QUE OCURRA EN LA API
        profileViewModel.getErrorUserProfile().observe(getActivity(), new Observer<Throwable>() {
            @Override
            public void onChanged(Throwable throwable) {
                Toast.makeText(MyApp.getContext(), throwable.getMessage(), Toast.LENGTH_LONG).show();
                enableButton(btnSaveProfile);
            }
        });

        profileViewModel.getUploadPhotoUserprofile().observe(getActivity(), new Observer<String>() {
            @Override
            public void onChanged(String photoName) {
                if(photoName.isEmpty()) { return ;}
                Glide.with(MyApp.getContext()).load(Const.API_GET_PHOTO_USER+photoName)
                        .dontAnimate()
                        .diskCacheStrategy(DiskCacheStrategy.NONE)
                        .skipMemoryCache(true)
                        .into(imgPhotoProfile);
            }
        });

        return v;
    }

    private void enableButton(Button btn) {
        btn.setEnabled(true);
        btn.getBackground().setColorFilter(Color.BLUE, PorterDuff.Mode.MULTIPLY);
    }

    private void disableButton(Button btn) {
        btn.setEnabled(false);
        btn.getBackground().setColorFilter(Color.GRAY, PorterDuff.Mode.MULTIPLY);
    }

    private void checkPermission() {
        PermissionListener dialogOnPermisionDenied =
                DialogOnDeniedPermissionListener.Builder.withContext(getActivity())
                .withTitle("Permisos")
                .withMessage("Los permisos solicitados son necesarios para poder seleccionar foto de perfil")
                .withButtonText("Aceptar")
                .withIcon(R.mipmap.ic_launcher)
                .build();

        permissionListener = new CompositePermissionListener(
                (PermissionListener)getActivity(),
                dialogOnPermisionDenied
        );

        Dexter.withActivity(getActivity()).withPermission(Manifest
                .permission.READ_EXTERNAL_STORAGE)
                .withListener(permissionListener)
                .check();
    }
}
