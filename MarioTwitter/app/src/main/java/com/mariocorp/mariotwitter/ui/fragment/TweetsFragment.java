package com.mariocorp.mariotwitter.ui.fragment;

import android.content.Context;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mariocorp.mariotwitter.R;
import com.mariocorp.mariotwitter.common.Const;
import com.mariocorp.mariotwitter.data.adapter.MyTweetRecyclerViewAdapter;
import com.mariocorp.mariotwitter.data.dto.response.Tweet;
import com.mariocorp.mariotwitter.data.viewModel.TweetViewModel;

import java.util.ArrayList;
import java.util.List;

public class TweetsFragment extends Fragment {
    private RecyclerView recyclerView;
    private MyTweetRecyclerViewAdapter adapter;
    TweetViewModel tweetViewModel;
    private SwipeRefreshLayout swipeRefreshLayout;
    private List<Tweet> tweetList = new ArrayList<Tweet>();
    private int mTweetListType = 1;

    public TweetsFragment() { }

    public static TweetsFragment newInstance(int tweetListType) {
        TweetsFragment fragment = new TweetsFragment();
        Bundle args = new Bundle();
        args.putInt(Const.TWEET_LIST_TYPE, tweetListType);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        tweetViewModel = ViewModelProviders.of(getActivity()) .get(TweetViewModel.class);
        if (getArguments() != null) {
            mTweetListType = getArguments().getInt(Const.TWEET_LIST_TYPE);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_tweets_list, container, false);
        //if (view instanceof RecyclerView) {
        Context context = view.getContext();
        recyclerView = view.findViewById(R.id.list);  //recyclerView = (RecyclerView) view;
        swipeRefreshLayout = view.findViewById(R.id.list_swiper_refresh_layout);
        swipeRefreshLayout.setColorSchemeColors(getResources().getColor(R.color.colorAzul));
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override public void onRefresh() {
                swipeRefreshLayout.setRefreshing(true);
                if( mTweetListType == Const.TWEET_LIST_ALL) {
                    doRequestTweets();
                } else if ( mTweetListType == Const.TWEET_LIST_FAVS) {
                    doRequestFavsTweets();
                }

            }
        });

        recyclerView.setLayoutManager(new LinearLayoutManager(context));

        adapter = new MyTweetRecyclerViewAdapter(getActivity(), tweetList);
        recyclerView.setAdapter(adapter);
        if( mTweetListType == Const.TWEET_LIST_ALL) {
            loadLocalTweets();
        } else if ( mTweetListType == Const.TWEET_LIST_FAVS) {
            loadLocalFavsTweets();
        }
        return view;
    }

    private void loadLocalTweets() {
        // Observar que tweetViewModel es el que hace la peticion al server
        tweetViewModel.getTweetsLiveData().observe(getActivity(), new Observer<List<Tweet>>() {
            @Override
            public void onChanged(List<Tweet> tweetListUpdate) {
                tweetList = tweetListUpdate;
                //indicamos al adapter que ahi un cambio
                adapter.setData(tweetList);
                swipeRefreshLayout.setRefreshing(false);
            }
        });
    }

    private void doRequestTweets() {
        // Observar que tweetViewModel es el que hace la peticion al server
        tweetViewModel.getNewTweetsLiveData().observe(getActivity(), new Observer<List<Tweet>>() {
            @Override
            public void onChanged(List<Tweet> tweetListUpdate) {
                tweetList = tweetListUpdate;
                //indicamos al adapter que ahi un cambio
                adapter.setData(tweetList);
                swipeRefreshLayout.setRefreshing(false);
                //EVITAMOS QUE SE DISPAREN 2 OBSERVERS QUITANDO ESTE OBSERVER PARA QUE CUANDO EJECUTEMOS EL NEW TWEET NO PASE ESTO
                tweetViewModel.getNewTweetsLiveData().removeObserver(this);
            }
        });
    }

    private void doRequestFavsTweets() {
        tweetViewModel.getNewFavTweets().observe(getActivity(), new Observer<List<Tweet>>() {
            @Override
            public void onChanged(List<Tweet> tweets) {
                tweetList = tweets;
                swipeRefreshLayout.setRefreshing(false);
                adapter.setData(tweetList);
                tweetViewModel.getNewFavTweets().removeObserver(this);
            }
        });
    }

    private void loadLocalFavsTweets() {
        tweetViewModel.getFavTweets().observe(getActivity(), new Observer<List<Tweet>>() {
            @Override
            public void onChanged(List<Tweet> tweets) {
                tweetList = tweets;
                adapter.setData(tweetList);
            }
        });
    }
}
