package com.mariocorp.mariotwitter.retrofit.auth;

import com.mariocorp.mariotwitter.data.dto.request.CreateTweetRequest;
import com.mariocorp.mariotwitter.data.dto.response.ChangeProfileRequest;
import com.mariocorp.mariotwitter.data.dto.response.DeleteTweetResponse;
import com.mariocorp.mariotwitter.data.dto.response.GetProfileResponse;
import com.mariocorp.mariotwitter.data.dto.response.UploadPhotoResponse;

import com.mariocorp.mariotwitter.data.dto.response.Tweet;

import java.util.List;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import retrofit2.http.Path;

public interface MiniTwitterServiceAuth {

    @GET("tweets/all")
    Call<List<Tweet>> getAllTweets();

    @POST("tweets/create")
    Call<Tweet> createTweet(@Body CreateTweetRequest addTweetRequest);

    @POST("tweets/like/{tweet_id}")
    Call<Tweet> likeTweet(@Path("tweet_id") int tweetId);

    @DELETE("tweets/{tweet_id}")
    Call<DeleteTweetResponse> deleteTweet(@Path("tweet_id") int tweetId);

    //Users
    @GET("users/profile")
    Call<GetProfileResponse> getProfile();

    @PUT("users/profile")
    Call<GetProfileResponse> changeProfile(@Body ChangeProfileRequest changeProfileRequest);

    @Multipart
    @POST("users/uploadprofilephoto")
    Call<UploadPhotoResponse> uploadPhotoProfile(@Part("file\"; filename=\"photo.jpeg\" ") RequestBody requestBody);

}
