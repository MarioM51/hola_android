package com.mariocorp.mariotwitter.data.viewModel;

import android.app.Application;
import android.content.Context;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.mariocorp.mariotwitter.data.dto.response.Tweet;
import com.mariocorp.mariotwitter.data.repository.TweetRepository;
import com.mariocorp.mariotwitter.ui.fragment.BottonModalTweetFragment;

import java.util.List;

public class TweetViewModel extends AndroidViewModel {
    private TweetRepository tweetRepository;
    private LiveData<List<Tweet>> tweetsLiveData;
    private LiveData<List<Tweet>> favTweetsLiveData;

    public TweetViewModel(@NonNull Application application) {
        super(application);
        tweetRepository = new TweetRepository();
        tweetsLiveData = tweetRepository.getAllTeetsLiveData();
    }

    public LiveData<List<Tweet>> getTweetsLiveData() {
        return tweetsLiveData;
    }

    public LiveData<List<Tweet>> getNewTweetsLiveData() {
        tweetsLiveData = tweetRepository.getAllTeetsLiveData();
        return tweetsLiveData;
    }

    public void doCreateTweet(String mensaje) {
        tweetRepository.createTweet(mensaje);
    }

    public void doLikeTweet(int idTweet) {
        tweetRepository.likeTweet(idTweet);
    }

    public LiveData<List<Tweet>> getFavTweets() {
        favTweetsLiveData = tweetRepository.getFavTeetsLiveData();
        return favTweetsLiveData;
    }

    public LiveData<List<Tweet>> getNewFavTweets() {
        getNewTweetsLiveData(); //OBTENEMOS DEL SERVIDOR TODOS LOS TWEETS
        return getFavTweets();  //APARTE DE OBTENER LOS TWEETS FAVORITOS LOS FILTRARA DE NUEVO
    }

    public void deleteTweet(int id) {
        tweetRepository.deleteTweet(id);
    }

    public void openDialogTweetMenu(Context context, int idTweet) {
        BottonModalTweetFragment bottonModalTweetFragment = BottonModalTweetFragment.newInstance(idTweet);
        bottonModalTweetFragment.show(((AppCompatActivity)context).getSupportFragmentManager(), "bottonModalTweetFragment");
    }
}
