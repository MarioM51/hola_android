package com.mariocorp.mariotwitter.common;

import android.content.Context;
import android.content.SharedPreferences;

import com.mariocorp.mariotwitter.data.dto.response.User;

public class SharedPreferencesManager {
    private static final String APP_SETTINGS_FILE = "APP_SETTINGS";
    private static User userLogged = null;

    private static SharedPreferences getSharedPreferences() {
        return MyApp.getContext().getSharedPreferences(APP_SETTINGS_FILE, Context.MODE_PRIVATE);
    }

    public static void saveString(String property, String value) {
        SharedPreferences.Editor editor = getSharedPreferences().edit();
        editor.putString(property, value);
        editor.commit();
    }

    public static void saveBoolean(String property, boolean value) {
        SharedPreferences.Editor editor = getSharedPreferences().edit();
        editor.putBoolean(property, value);
        editor.commit();
    }

    public static void saveInteger(String property, int value) {
        SharedPreferences.Editor editor = getSharedPreferences().edit();
        editor.putInt(property, value);
        editor.commit();
    }

    public static User saveUserData(String username, String email, String photoUrl, String created, boolean active, int id) {
        saveInteger(Const.USER_ID, id);
        saveString(Const.USER_USERNAME, username);
        saveString(Const.USER_EMAIL, email);
        saveString(Const.USER_PHOTOURL, photoUrl);
        saveString(Const.USER_CREATED, created);
        saveBoolean(Const.USER_ACTIVE, active);
        userLogged = new User(id, username, "falta guardar descipcion", "falta website", photoUrl, created);
        return userLogged;
    }

    public static String getsomeString(final String property) {
        return getSharedPreferences().getString(property, null);
    }

    public static boolean getsomeBoolean(final String property) {
        return getSharedPreferences().getBoolean(property, false);
    }
    public static User getLogedUser() {
        return userLogged;
    }

}
