package com.mariocorp.mariotwitter.retrofit;

import com.mariocorp.mariotwitter.common.Const;
import com.mariocorp.mariotwitter.common.SharedPreferencesManager;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

public class AuthInterceptor implements Interceptor {
    @Override
    public Response intercept(Chain chain) throws IOException {
        String token = SharedPreferencesManager.getsomeString(Const.TOKEN);
        String propertie = "Authorization";
        String value = "Bearer " + token;
        Request request = chain.request().newBuilder().addHeader(propertie, value).build();
        return chain.proceed(request);
    }
}
