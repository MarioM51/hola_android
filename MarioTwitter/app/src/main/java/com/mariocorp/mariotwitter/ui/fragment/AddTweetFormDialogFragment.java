package com.mariocorp.mariotwitter.ui.fragment;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.DialogFragment;
import androidx.lifecycle.ViewModelProviders;

import com.bumptech.glide.Glide;
import com.mariocorp.mariotwitter.R;
import com.mariocorp.mariotwitter.common.Const;
import com.mariocorp.mariotwitter.common.SharedPreferencesManager;
import com.mariocorp.mariotwitter.data.viewModel.TweetViewModel;

public class AddTweetFormDialogFragment extends DialogFragment implements View.OnClickListener {
    ImageView btnClose, imgPhotoUser;
    Button btnDoCreateTweet;
    EditText tweetMesaje;


    @Override public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NORMAL, R.style.FullScreenDialogStyle);
    }

    @Nullable @Override public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View v = inflater.inflate(R.layout.add_tweet_form_dialog_fragment, container, false);

        btnClose = v.findViewById(R.id.imageView_close_form_add_tweet);
        btnClose.setOnClickListener(this);

        btnDoCreateTweet = v.findViewById(R.id.btn_do_twittear);
        btnDoCreateTweet.setOnClickListener(this);

        imgPhotoUser = v.findViewById(R.id.imageView_form_add_tweet_photo_user);
        String photoUrl = SharedPreferencesManager.getsomeString(Const.USER_PHOTOURL);
        if(!photoUrl.isEmpty()) {
            Glide.with(getActivity()).load(Const.API_GET_PHOTO_USER + photoUrl).into(imgPhotoUser);
        }
        tweetMesaje = v.findViewById(R.id.editText_add_tweet_msj);
        return v;
    }

    @Override public void onClick(View v) {
        String msj = tweetMesaje.getText().toString();
        switch (v.getId()) {
            case R.id.btn_do_twittear:
                if(msj.isEmpty()) { tweetMesaje.setError("El Tweet no puede estar vacio"); return ; }
                TweetViewModel tweetViewModel = ViewModelProviders.of(getActivity()).get(TweetViewModel.class);
                tweetViewModel.doCreateTweet(msj);
                getDialog().dismiss();
            break;

            case R.id.imageView_close_form_add_tweet:
                if(!msj.isEmpty()) { showDialogCloseConfirm(); }
                else {               getDialog().dismiss();    }
            break;
        }

    }

    private void showDialogCloseConfirm() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage("¿Realmente quieres cancelar el tweet?").setTitle("No publicar Tweet");
        builder.setPositiveButton("Eliminar", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
    //CERRAMOS EL DILOGO DE CONFIRMACION Y DE CREAR TWEET
            dialog.dismiss();
            getDialog().dismiss();
            }
        });
        builder.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                // CUIDADO getDialog().dismiss();
                dialog.dismiss();
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }
}
