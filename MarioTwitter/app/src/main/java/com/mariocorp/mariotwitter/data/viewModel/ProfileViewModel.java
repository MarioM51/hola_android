package com.mariocorp.mariotwitter.data.viewModel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

import com.mariocorp.mariotwitter.data.dto.response.ChangeProfileRequest;
import com.mariocorp.mariotwitter.data.dto.response.GetProfileResponse;
import com.mariocorp.mariotwitter.data.repository.ProfileRepository;

public class ProfileViewModel extends AndroidViewModel {
    private ProfileRepository profileRepository;
    private MutableLiveData<GetProfileResponse> userprofile;
    private MutableLiveData<String> photoUserprofile;

    public ProfileViewModel(@NonNull Application application) {
        super(application);
        profileRepository = new ProfileRepository();
        userprofile = profileRepository.getUserprofile();
        photoUserprofile = profileRepository.getPhotoProfile();
    }

    public MutableLiveData<GetProfileResponse> getUserprofile() {
        return userprofile;
    }

    public MutableLiveData<GetProfileResponse> changeUserprofile(ChangeProfileRequest changeProfileRequest) {
        return profileRepository.chnageUserprofile(changeProfileRequest);
    }

    public MutableLiveData<Throwable> getErrorUserProfile() {
        return profileRepository.getError();
    }

    public MutableLiveData<String> uploadPhoto(String photo) {
        photoUserprofile = profileRepository.uploadPhoto(photo);
        return photoUserprofile;
    }

    public MutableLiveData<String> getUploadPhotoUserprofile() {
        return photoUserprofile;
    }

}
