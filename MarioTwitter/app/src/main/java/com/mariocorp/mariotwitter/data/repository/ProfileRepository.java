package com.mariocorp.mariotwitter.data.repository;

import android.util.Log;
import android.widget.Toast;

import androidx.lifecycle.MutableLiveData;

import com.mariocorp.mariotwitter.common.Const;
import com.mariocorp.mariotwitter.common.MyApp;
import com.mariocorp.mariotwitter.common.SharedPreferencesManager;
import com.mariocorp.mariotwitter.data.dto.response.ChangeProfileRequest;
import com.mariocorp.mariotwitter.data.dto.response.GetProfileResponse;
import com.mariocorp.mariotwitter.data.dto.response.UploadPhotoResponse;
import com.mariocorp.mariotwitter.retrofit.auth.MiniTwitterClientAuth;
import com.mariocorp.mariotwitter.retrofit.auth.MiniTwitterServiceAuth;

import java.io.File;


import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProfileRepository {
    private final String TAG = this.getClass().getSimpleName();

    private MiniTwitterClientAuth miniTwitterClientAuth;
    private MiniTwitterServiceAuth miniTwitterServiceAuth;
    private MutableLiveData<GetProfileResponse> userprofile;
    final private MutableLiveData<Throwable> error = new MutableLiveData<>();
    final private MutableLiveData<String> photoProfile;

    public ProfileRepository() {
        retrofitInit();
        userprofile = getUserprofile();
        photoProfile = new MutableLiveData<String>();
    }

    private void retrofitInit() {
        miniTwitterClientAuth = MiniTwitterClientAuth.getInstance();
        miniTwitterServiceAuth = miniTwitterClientAuth.getMiniTwitterServiceAuth();
    }

    public MutableLiveData<GetProfileResponse> getUserprofile() {
        if(userprofile == null) {
            userprofile = new MutableLiveData<>();
        }

        Call<GetProfileResponse> call = miniTwitterServiceAuth.getProfile();
        call.enqueue(new Callback<GetProfileResponse>() {
            @Override public void onResponse(Call<GetProfileResponse> call, Response<GetProfileResponse> response) {
                if(response.isSuccessful()) {
                    userprofile.setValue(response.body());
                } else {
                    Toast.makeText(MyApp.getContext(), "Error de peticion", Toast.LENGTH_LONG).show();
                }
            }
            @Override public void onFailure(Call<GetProfileResponse> call, Throwable t) {
                Toast.makeText(MyApp.getContext(), "Error de conexion", Toast.LENGTH_LONG).show();
            }
        });
        return userprofile;
    }


    public MutableLiveData<GetProfileResponse> chnageUserprofile(ChangeProfileRequest changeProfileRequest) {
        if(userprofile == null) {
            userprofile = new MutableLiveData<>();
        }
        Call<GetProfileResponse> call = miniTwitterServiceAuth.changeProfile(changeProfileRequest);
        call.enqueue(new Callback<GetProfileResponse>() {
            @Override public void onResponse(Call<GetProfileResponse> call, Response<GetProfileResponse> response) {
                if(response.isSuccessful()) {
                    userprofile.setValue(response.body());
                } else {
                    error.setValue(new Throwable("Error de peticion"));
                }
            }
            @Override public void onFailure(Call<GetProfileResponse> call, Throwable t) {
                error.setValue(t);
            }
        });
        return userprofile;
    }

    public MutableLiveData<Throwable> getError() {
        return error;
    }

    public MutableLiveData<String> uploadPhoto(String photoPath) {
        File file = new File(photoPath);
        RequestBody requestBody = RequestBody.create(MediaType.parse("image/jpg"), file);
        Call<UploadPhotoResponse> uploadPhotoResponse = miniTwitterServiceAuth.uploadPhotoProfile(requestBody);
        uploadPhotoResponse.enqueue(new Callback<UploadPhotoResponse>() {
            @Override
            public void onResponse(Call<UploadPhotoResponse> call, Response<UploadPhotoResponse> response) {
                if(response.isSuccessful()) {
                    String filename = response.body().getFilename();
                    SharedPreferencesManager.saveString(Const.USER_PHOTOURL, filename);
                    photoProfile.setValue(filename);
                } else {
                    error.setValue(new Throwable("Error en la peticion"));
                }
            }
            @Override
            public void onFailure(Call<UploadPhotoResponse> call, Throwable t) {
                error.setValue(new Throwable(t));
            }
        });
        return photoProfile;
    }

    public MutableLiveData<String> getPhotoProfile() {
        return photoProfile;
    }

}
