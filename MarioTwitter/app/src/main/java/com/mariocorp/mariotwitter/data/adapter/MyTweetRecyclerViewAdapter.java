package com.mariocorp.mariotwitter.data.adapter;

import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.mariocorp.mariotwitter.R;
import com.mariocorp.mariotwitter.common.Const;
import com.mariocorp.mariotwitter.common.SharedPreferencesManager;
import com.mariocorp.mariotwitter.data.dto.response.Like;
import com.mariocorp.mariotwitter.data.dto.response.Tweet;
import com.mariocorp.mariotwitter.data.viewModel.TweetViewModel;

import java.util.List;

public class MyTweetRecyclerViewAdapter extends RecyclerView.Adapter<MyTweetRecyclerViewAdapter.ViewHolder> {
    private List<Tweet> mValues;
    private Context ctx;
    private final String username = SharedPreferencesManager.getsomeString(Const.USER_USERNAME);
    private TweetViewModel tweetViewModel;

    public MyTweetRecyclerViewAdapter(Context context, List<Tweet> items) {
        mValues = items;
        ctx = context;
        tweetViewModel = ViewModelProviders.of((FragmentActivity) ctx).get(TweetViewModel.class);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.fragment_tweets, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        if(mValues == null) {
            return ; //si NO hay datos no tendremos por que mostrar algo
        }

        holder.mItem = mValues.get(position);

        holder.userName.setText(holder.mItem.getUser().getUsername());
        holder.text.setText(holder.mItem.getMensaje());
        holder.numLikss.setText(String.valueOf(holder.mItem.getLikes().size()));

        holder.icLike.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                tweetViewModel.doLikeTweet(holder.mItem.getId());
            }
        });

        holder.icSHowMenu.setVisibility(View.GONE);
        if(holder.mItem.getUser().getUsername().equals(username)) {
            holder.icSHowMenu.setVisibility(View.VISIBLE);
        }
        holder.icSHowMenu.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                int idDelete = holder.mItem.getId();
                tweetViewModel.openDialogTweetMenu(ctx, idDelete);
            }
        });

        //cargamos la foto de usuario de internet
        String photo = holder.mItem.getUser().getPhotoUrl();
        if(!photo.equals("")) {
            String url = Const.API_GET_PHOTO_USER + photo;
            Glide.with(ctx)
                    .load(url)
                    .dontAnimate()
                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                    .skipMemoryCache(true)
                    .into(holder.userPhoto);
        }
        //buscamos si el usuario le dio like, para pintar el icono de like y el numero de likes vacio y/o rosa
        for(Like like: holder.mItem.getLikes()) {
            if(like.getUsername().equals(username)) {
                Glide.with(ctx).load(R.drawable.ic_like_pink).into(holder.icLike);
                holder.numLikss.setTextColor(ctx.getResources().getColor(R.color.colorRosa));
                holder.numLikss.setTypeface(null, Typeface.BOLD);
                break;
            }
        }
    }

    @Override
    public int getItemCount() {
        if(mValues == null) { return 0; }
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public TextView text, userName, numLikss;
        public ImageView userPhoto, icLike, icSHowMenu;
        public Tweet mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;

            userPhoto = view.findViewById(R.id.imageView_tweet_photo_user);
            icLike = view.findViewById(R.id.imageView_tweet_like);
            userName = view.findViewById(R.id.textView_tweet_user_name);
            text = view.findViewById(R.id.textView_tweet_text);
            numLikss = view.findViewById(R.id.textView_tweet_num_likes);
            icSHowMenu = view.findViewById(R.id.ic_show_menu);
        }
    }

    public void setData(List<Tweet> tweets) {
        mValues = tweets;
        notifyDataSetChanged();
    }

}
