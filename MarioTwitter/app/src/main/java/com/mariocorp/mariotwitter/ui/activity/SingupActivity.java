package com.mariocorp.mariotwitter.ui.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.mariocorp.mariotwitter.R;
import com.mariocorp.mariotwitter.data.dto.response.LoginResponse;
import com.mariocorp.mariotwitter.data.dto.request.SingupRequest;
import com.mariocorp.mariotwitter.retrofit.MiniTwitterClient;
import com.mariocorp.mariotwitter.retrofit.MiniTwitterService;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SingupActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String TAG = "SingupActivity";
    private EditText editTextRgtEmail, editTextRgtPassword, editTextRgtNombre;
    private Button btnDoSingUp;

    //api
    private MiniTwitterClient miniTwitterClient;
    private MiniTwitterService miniTwitterService;

    private void setuptUI() {
        editTextRgtEmail = findViewById(R.id.editTextRgtEmail);
        editTextRgtPassword = findViewById(R.id.editTextRgtPassword);
        editTextRgtNombre = findViewById(R.id.editTextRgtNombre);

        btnDoSingUp = findViewById(R.id.btn_do_singup);
        btnDoSingUp.setOnClickListener(this);
    }

    private void retrofitInit() {
        miniTwitterClient = MiniTwitterClient.getInstance();
        miniTwitterService = miniTwitterClient.getMiniTwitterService();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_singup);
        retrofitInit();
        setuptUI();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_do_singup:
                this.doSingup();
                break;
        }
    }

    private void doSingup() {

        String inputEmail =  editTextRgtEmail.getText().toString();
        String inputPass =  editTextRgtPassword.getText().toString();
        String inputNombre =  editTextRgtNombre.getText().toString();

        Log.i(TAG, String.format("To register nombre: %s email: %s | pass: %s", inputNombre, inputEmail, inputPass));

        if(inputEmail.isEmpty()) { editTextRgtEmail.setError("El email es requerido");       return ; }
        if(inputPass.isEmpty()) {  editTextRgtPassword.setError("El password es requerido"); return ; }
        if(inputNombre.isEmpty()) {  editTextRgtNombre.setError("El nombre es requerido"); return ; }

        SingupRequest singupRequest = new SingupRequest(inputNombre, inputEmail, inputPass);
        Call<LoginResponse> call = miniTwitterService.doSingup(singupRequest);
        call.enqueue(new Callback<LoginResponse>() {
            @Override public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                if(response.isSuccessful()) {//esta en el rango del 200
                    Intent i = new Intent(SingupActivity.this, MainActivity.class);
                    startActivity(i);
                    finish(); // lo terminamos ya que el usuario no requerira volver al login
                } else {
                    Toast.makeText(SingupActivity.this, "Reviza los datos", Toast.LENGTH_LONG).show();
                }
            }

            @Override public void onFailure(Call<LoginResponse> call, Throwable t) {
                Toast.makeText(SingupActivity.this, "Problemas de conexion", Toast.LENGTH_LONG).show();
            }
        });

    }
}

