package com.mariocorp.mariotwitter.data.dto.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ChangeProfileRequest {

    @SerializedName("username") @Expose private String      username;
    @SerializedName("email") @Expose private String         email;
    @SerializedName("descripcion") @Expose private String   descripcion;
    @SerializedName("website") @Expose private String       website;
    @SerializedName("password") @Expose private String      pasword;

    public ChangeProfileRequest() { }

    public ChangeProfileRequest(String username, String email, String descripcion, String website) {
        super();
        this.username = username;
        this.email = email;
        this.descripcion = descripcion;
        this.website = website;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getPasword() { return pasword; }

    public void setPasword(String pasword) { this.pasword = pasword; }
}