package com.mariocorp.mariotwitter.retrofit;

import com.mariocorp.mariotwitter.data.dto.request.LoginRequest;
import com.mariocorp.mariotwitter.data.dto.response.LoginResponse;
import com.mariocorp.mariotwitter.data.dto.request.SingupRequest;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface MiniTwitterService {

    @POST("auth/login")
    Call<LoginResponse> doLogin(@Body LoginRequest loginRequest);

    @POST("auth/signup")
    Call<LoginResponse> doSingup(@Body SingupRequest singupRequest);
}
