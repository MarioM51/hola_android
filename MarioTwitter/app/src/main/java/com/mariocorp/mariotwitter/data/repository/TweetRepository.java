package com.mariocorp.mariotwitter.data.repository;

import android.util.Log;
import android.widget.Toast;

import androidx.lifecycle.MutableLiveData;

import com.mariocorp.mariotwitter.common.Const;
import com.mariocorp.mariotwitter.common.MyApp;
import com.mariocorp.mariotwitter.common.SharedPreferencesManager;
import com.mariocorp.mariotwitter.data.dto.request.CreateTweetRequest;
import com.mariocorp.mariotwitter.data.dto.response.DeleteTweetResponse;
import com.mariocorp.mariotwitter.data.dto.response.Like;
import com.mariocorp.mariotwitter.data.dto.response.Tweet;
import com.mariocorp.mariotwitter.retrofit.auth.MiniTwitterClientAuth;
import com.mariocorp.mariotwitter.retrofit.auth.MiniTwitterServiceAuth;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TweetRepository {
    private MiniTwitterClientAuth miniTwitterClientAuth;
    private MiniTwitterServiceAuth miniTwitterServiceAuth;
    private MutableLiveData<List<Tweet>> allTeetsLiveData;
    private MutableLiveData<List<Tweet>> favTeetsLiveData;
    String username;

    private  static final String TAG = "TweetRepository";

    public TweetRepository() {
        retrofitInit();
        allTeetsLiveData = getAllTeetsLiveData();
        username = SharedPreferencesManager.getsomeString(Const.USER_USERNAME);
    }

    private void retrofitInit() {
        miniTwitterClientAuth = MiniTwitterClientAuth.getInstance();
        miniTwitterServiceAuth = miniTwitterClientAuth.getMiniTwitterServiceAuth();
    }

    public MutableLiveData<List<Tweet>> getAllTeetsLiveData() {
        if(allTeetsLiveData == null) {
            allTeetsLiveData = new MutableLiveData<>();
        }

        // CUIDADO final MutableLiveData<List<Tweet>> allTeetsLiveData = new MutableLiveData<>();
        Call<List<Tweet>> call = miniTwitterServiceAuth.getAllTweets();
        call.enqueue(new Callback<List<Tweet>>() {
            @Override public void onResponse(Call<List<Tweet>> call, Response<List<Tweet>> response) {
                if(response.isSuccessful()) {
                    //PASAMOS LOS TWEETS QUE LLGARON DEL SERVIDOR AL ADAPTADOR
                    allTeetsLiveData.setValue(response.body());
                } else {
                    Log.e(TAG, "Error de peticion");
                }
            }

            @Override public void onFailure(Call<List<Tweet>> call, Throwable t) {
                Log.e(TAG, "Error de conexion");
            }
        });
        return allTeetsLiveData;
    }

    public MutableLiveData<List<Tweet>> getFavTeetsLiveData() {
        if (favTeetsLiveData == null) {
            favTeetsLiveData = new MutableLiveData<>();
        }

        //BUSCAMOS LOS TWEETS QUE EL USUARIO ACTUAL LE DIO LIKE
        List<Tweet> newFavList = new ArrayList<>();
        Iterator itTweets  = allTeetsLiveData.getValue().iterator();
        while(itTweets.hasNext()) {
            Tweet curretTweet = (Tweet)itTweets.next();
            Iterator itLikes = curretTweet.getLikes().iterator();
            boolean finded = false;
            while (itLikes.hasNext() && !finded) {//UN TWEET TIENE VARIOS LIKES
                Like currentLike = (Like)itLikes.next();
                if(currentLike.getUsername().equals(username)) {
                    // DENTRO DE LOS LIKES AHI UNO QUE TIENE EL MISMO NOMBRE DE USUARIO QUE EL LOGEADO
                    finded = true;
                    newFavList.add(curretTweet);
                }
            }
        }
        favTeetsLiveData.setValue(newFavList);//NOTIFICAMOS DE CAMBIO A UI
        return favTeetsLiveData;
    }

    public void createTweet(String msj) {
         /*// PRUEBA SIN MANDAR PETICION
        List<Tweet> listaClonada = new ArrayList<>();
        Tweet tweetCreated = new Tweet(111, msj, new ArrayList<Like>(), SharedPreferencesManager.getLogedUser());
        listaClonada.add(tweetCreated);
        for (int i = 0; i < allTeetsLiveData.getValue().size(); i++) {
            Tweet tweetClon =  new Tweet(allTeetsLiveData.getValue().get(i));
            listaClonada.add(tweetClon);
        }
        allTeetsLiveData.setValue(listaClonada);
        */
        CreateTweetRequest createTweetRequest = new CreateTweetRequest(msj);
        Call<Tweet> tweetCall = miniTwitterServiceAuth.createTweet(createTweetRequest);
        tweetCall.enqueue(new Callback<Tweet>() {
            @Override public void onResponse(Call<Tweet> call, Response<Tweet> response) {
                if(response.isSuccessful()){
                    List<Tweet> listaClonada = new ArrayList<>();
                    Tweet tweetCreated = response.body();
                    listaClonada.add(tweetCreated);
                    for (int i = 0; i < allTeetsLiveData.getValue().size(); i++) {
                        Tweet tweetClon =  new Tweet(allTeetsLiveData.getValue().get(i));
                        listaClonada.add(tweetClon);
                    }
                    allTeetsLiveData.setValue(listaClonada);
                    Log.i(TAG, "Lista clonada por creacion de tweet");
                } else {
                    Toast.makeText(MyApp.getContext(), "Error, Checa tu peticion", Toast.LENGTH_LONG).show();
                }
            }
            @Override public void onFailure(Call<Tweet> call, Throwable t) {
                Toast.makeText(MyApp.getContext(), "Error de conexion", Toast.LENGTH_LONG).show();
            }
        });
    }


    public void likeTweet(final int idTweet) {
        Call<Tweet> tweetCall = miniTwitterServiceAuth.likeTweet(idTweet);
        tweetCall.enqueue(new Callback<Tweet>() {
            @Override public void onResponse(Call<Tweet> call, Response<Tweet> response) {
                if(response.isSuccessful()){
                    List<Tweet> listaClonada = new ArrayList<>();
                    for (int i = 0; i < allTeetsLiveData.getValue().size(); i++) {
                        if(allTeetsLiveData.getValue().get(i).getId() == idTweet)  {
                            Tweet tweetLiked = response.body();
                            listaClonada.add(tweetLiked);
                        } else {
                            Tweet tweetClon = allTeetsLiveData.getValue().get(i);
                            listaClonada.add(tweetClon);
                        }
                    }
                    allTeetsLiveData.setValue(listaClonada);//AVISAMOS DE CAMBIO AL UI
                    getFavTeetsLiveData();//REFRESCAMOS LA LISTA DE FAVORITOS POR SI LE DIO ME GUSTA A UN TWEET PROPIP
                    Log.i(TAG, "Lista clonada por like a tweet");
                } else {
                    Toast.makeText(MyApp.getContext(), "Error, Checa tu peticion", Toast.LENGTH_LONG).show();
                }
            }
            @Override public void onFailure(Call<Tweet> call, Throwable t) {
                Toast.makeText(MyApp.getContext(), "Error de conexion", Toast.LENGTH_LONG).show();
            }
        });
    }

    public void deleteTweet(final int idTweet) {
        Call<DeleteTweetResponse> deleteTweetResponseCall = miniTwitterServiceAuth.deleteTweet(idTweet);
        deleteTweetResponseCall.enqueue(new Callback<DeleteTweetResponse>() {
            @Override public void onResponse(Call<DeleteTweetResponse> call, Response<DeleteTweetResponse> response) {
                if(response.isSuccessful()){
                    List<Tweet> listaClonada = new ArrayList<>();
                    for (int i = 0; i < allTeetsLiveData.getValue().size(); i++) {
                        if(allTeetsLiveData.getValue().get(i).getId() != idTweet)  {
            //SI EL ID DEL TWEET ELIMINADO ES EL MISMO AL RECODRRIDO
            //DE TODA LA LISTA NO LO INCLUIMOS EN LA LISTA CLONADA
                            listaClonada.add(allTeetsLiveData.getValue().get(i));
                        }
                    }
                    allTeetsLiveData.setValue(listaClonada);//AVISAMOS DE CAMBIO AL UI
                    getFavTeetsLiveData();//REFRESCAMOS LA LISTA DE FAVORITOS POR SI LE DIO ME GUSTA A UN TWEET PROPIP
                    Log.i(TAG, "Lista clonada por like eliminado");
                } else {
                    Toast.makeText(MyApp.getContext(), "Error, Checa tu peticion", Toast.LENGTH_LONG).show();
                }
            }
            @Override public void onFailure(Call<DeleteTweetResponse> call, Throwable t) {
                Toast.makeText(MyApp.getContext(), "Error de conexion", Toast.LENGTH_LONG).show();
            }
        });
    }


}
