package com.mariocorp.mariotwitter.common;

import android.os.Bundle;

public class Const {
    public static final String API_MINI_TWITTER_BASE = "https://www.minitwitter.com:3001/apiv1/";
    public static final String API_SINGUP_CODE = "UDEMYANDROID";
    public static final String API_GET_PHOTO_USER = "https://www.minitwitter.com/apiv1/uploads/photos/";

    public static final String TOKEN = "TOKEN";

    public static final String USER_ID = "USER_ID";
    public static final String USER_USERNAME = "USER_USERNAME";
    public static final String USER_EMAIL = "USER_EMAIL";
    public static final String USER_PHOTOURL = "USER_PHOTOURL";
    public static final String USER_CREATED = "USER_CREATE";
    public static final String USER_ACTIVE = "USER_ACTIVE";

    //argumnetos
    public static final String TWEET_LIST_TYPE = "TWEET_LIST_TYPE";
    public static final int TWEET_LIST_ALL = 1;
    public static final int TWEET_LIST_FAVS = 2;
    public static final String ARG_TWEET_ID = "ARG_TWEET_ID";

    //start activity
    public static final int SELECT_PHOTO = 1;
}
