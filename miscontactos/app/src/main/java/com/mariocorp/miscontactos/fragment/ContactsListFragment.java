package com.mariocorp.miscontactos.fragment;


import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.ListFragment;

import com.mariocorp.miscontactos.coms.IActivityComs;
import com.mariocorp.miscontactos.R;
import com.mariocorp.miscontactos.model.Contact;
import com.mariocorp.miscontactos.service.ContactManagger;

import java.util.ArrayList;

public class ContactsListFragment extends ListFragment {
    private ArrayList<Contact> contacts;

    private IActivityComs mActivityCons;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        contacts = ContactManagger.getInstance().getContacts(); //QUE TENGO QUE MOSTRAR
        ContactsListAdapter adapter = new ContactsListAdapter(contacts); // QUIEN SE ENCARGA DE MOSTRARLO
        setListAdapter(adapter);    // UNION
    }

    //POR AQUI NOS LLEGARA EL MAIN_ACTIVITY PERO LA USAREMOS COMO ActivityCons
    @Override public void onAttach(Context context) {
        super.onAttach(context);
        mActivityCons = (IActivityComs)context;
    }

    @Override public void onDetach() {
        super.onDetach();
        mActivityCons = null;
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        mActivityCons.onListItemSelected(position);
    }







//---------------adapter
    public class ContactsListAdapter extends ArrayAdapter<Contact> {


        public ContactsListAdapter(ArrayList<Contact> inContacts) {
            super(getActivity(), R.layout.adapter_list_contacts_item, inContacts);
        }

        @NonNull
        @Override
        public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
            LayoutInflater inflater = (LayoutInflater) getActivity().getLayoutInflater();
            if(convertView == null) {//SI NO SE A LLAMADO ANTES EL ADAPTER ESTE NO SE A LLENADO
                convertView = inflater.inflate(R.layout.adapter_list_contacts_item, null);
            }
            Contact currentContact = getItem(position);//COMO EXTIENDO DE ArrayAdapter PUEDO TENER EL ACTUAL ASI
            //ASIGNAMOS VALORES A MOSTRAR
            TextView txtName = convertView.findViewById(R.id.txt_item_contact_name);
            txtName.setText(currentContact.getName() +", " + currentContact.getAlias());

            return convertView;
        }
    }
}
