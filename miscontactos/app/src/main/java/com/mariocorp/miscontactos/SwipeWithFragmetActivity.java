package com.mariocorp.miscontactos;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.fragment.app.ListFragment;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import android.graphics.Color;
import android.os.Bundle;
import android.view.View;

import com.mariocorp.miscontactos.fragment.SwipeFragment;
import com.mariocorp.miscontactos.model.SwipeFragmentProperties;

import java.util.ArrayList;
import java.util.List;

public class SwipeWithFragmetActivity extends AppCompatActivity {

    private SwipeFragmentPagerAdapter swipeFragmentPagerAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_swipe_with_fragmet);
        //elementos a mostrar
        List<Fragment> fragmentList = new ArrayList<Fragment>();


        fragmentList.add(SwipeFragment.newInstance(
                new SwipeFragmentProperties(
                        "Primero mensaje",
                        Color.rgb(0, 210, 190), //fondo
                        Color.rgb(100, 27, 72), //img up
                        Color.rgb(200, 131, 72),    //imgdown
                        R.drawable.im1 //img
                    )
                )
        );
        fragmentList.add(SwipeFragment.newInstance(new SwipeFragmentProperties(
                        "Segundo Esta lloviendo",
                        Color.rgb(215, 250, 243), //fondo
                        Color.rgb(120, 180, 200), //img up
                        Color.rgb(200, 131, 72),//imgdown
                        R.drawable.im2 //img
                )
        ));
        fragmentList.add(SwipeFragment.newInstance(new SwipeFragmentProperties(
                        "Tersero esto se esta agregando rapido por que uso fragmentos y quiero ver textos largos",
                        Color.rgb(228, 213, 255), //fondo
                        Color.rgb(100, 27, 72), //img up
                        Color.rgb(201, 201, 29),    //imgdown
                        R.drawable.im3 //img
                )
        ));
        fragmentList.add(SwipeFragment.newInstance(new SwipeFragmentProperties(
                        "Hola y ... ",
                        Color.rgb(190, 0, 220), //fondo
                        Color.rgb(100, 27, 72), //img up
                        Color.rgb(15, 213, 211),    //imgdown
                        R.drawable.im4 //img
                )
        ));
        fragmentList.add(SwipeFragment.newInstance(new SwipeFragmentProperties(
                        "Adios",
                        Color.rgb(190, 210, 0), //fondo
                        Color.rgb(10, 27, 72), //img up
                        Color.rgb(20, 213, 207),    //imgdown
                        R.drawable.im5 //img
                )
        ));

        swipeFragmentPagerAdapter = new SwipeFragmentPagerAdapter(getSupportFragmentManager(), fragmentList);
        ViewPager pager = findViewById(R.id.pager_fragmets);
        pager.setAdapter(swipeFragmentPagerAdapter);

    }

    private class SwipeFragmentPagerAdapter extends FragmentPagerAdapter {
        List<Fragment> fragments;

        public SwipeFragmentPagerAdapter(FragmentManager fm, List<Fragment> fragments) {
            super(fm);
            this.fragments = fragments;
        }

        @Override
        public int getCount() { return fragments.size(); }

        @Override
        public Fragment getItem(int position) { return fragments.get(position); }

    }
}
