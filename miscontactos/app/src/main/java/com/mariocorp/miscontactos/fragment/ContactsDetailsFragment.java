package com.mariocorp.miscontactos.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.mariocorp.miscontactos.Constans;
import com.mariocorp.miscontactos.PortraitDetailsContactActivity;
import com.mariocorp.miscontactos.R;
import com.mariocorp.miscontactos.model.Contact;
import com.mariocorp.miscontactos.service.ContactManagger;

import java.util.ArrayList;

public class ContactsDetailsFragment extends Fragment {
    private ArrayList<Contact> mContacts;

    private Contact currentContact;

    public static ContactsDetailsFragment instantiate(int position){
        Bundle bundle = new Bundle();
        bundle.putInt(Constans.POSITION, position);
        ContactsDetailsFragment frag = new ContactsDetailsFragment();
        frag.setArguments(bundle);
        return frag;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.mContacts = ContactManagger.getInstance().getContacts();
        int position = getArguments().getInt(Constans.POSITION);
        this.currentContact = mContacts.get(position);
    }

    @Nullable @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_contact_details, container, false);
        TextView txtName = v.findViewById(R.id.frag_contact_details_name);
        TextView txtAlias = v.findViewById(R.id.frag_contact_details_alias);
        TextView txtNumber = v.findViewById(R.id.frag_contact_details_number);
        TextView txtnEmail = v.findViewById(R.id.frag_contact_details_email);
        txtName.setText(this.currentContact.getName());
        txtAlias.setText(this.currentContact.getAlias());
        txtNumber.setText(this.currentContact.getNumber());
        txtnEmail.setText(this.currentContact.getEmail());

        return v;
    }
}
