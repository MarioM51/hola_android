package com.mariocorp.miscontactos.service;

import com.mariocorp.miscontactos.model.Contact;

import java.util.ArrayList;

public class ContactManagger {
    //CREAMOS REFERENCIA INSTANCIA UNICA
    private static ContactManagger contactList;
    private ArrayList<Contact> contacts;//LO QUE GESTIONARA
    //OBTENEMOS UNICA INSTANCIA
    public static ContactManagger getInstance() {
        if(contactList == null) { return new ContactManagger(); }
        else { return contactList; }
    }
    //CONSTRUCTOR PRIVADO
    private ContactManagger() {
        contacts = new ArrayList<Contact>();
        createData();
    }

    public ArrayList<Contact> getContacts() {
        return contacts;
    }

    private void createData() {
        Contact c1 = new Contact("mario", "Tatemas", "2412345467", "mario@gmail.com");
        Contact c2 = new Contact("pancho", "Panchito", "1234567890", "pancho@mail.com");
        Contact c3 = new Contact("sutano", "Sutanito", "087654321", "sutano@mail.com");
        Contact c4 = new Contact("mengano", "Menganito", "7890654321", "mengano@mail.com");
        contacts.add(c1); contacts.add(c2); contacts.add(c3); contacts.add(c4);
    }
}
