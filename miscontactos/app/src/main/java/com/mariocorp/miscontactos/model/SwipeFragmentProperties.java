package com.mariocorp.miscontactos.model;

import android.graphics.Color;

import java.io.Serializable;

public class SwipeFragmentProperties implements Serializable {
    private String message;
    private int Rootbackgroud;
    private int BoxUo;
    private int BoxDown;
    private int imageResource;

    public SwipeFragmentProperties(String message, int rootbackgroud, int boxUo, int boxDown, int imageResource) {
        this.message = message;
        Rootbackgroud = rootbackgroud;
        BoxUo = boxUo;
        BoxDown = boxDown;
        this.imageResource = imageResource;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getRootbackgroud() {
        return Rootbackgroud;
    }

    public void setRootbackgroud(int rootbackgroud) {
        Rootbackgroud = rootbackgroud;
    }

    public int getBoxUo() {
        return BoxUo;
    }

    public void setBoxUo(int boxUo) {
        BoxUo = boxUo;
    }

    public int getBoxDown() {
        return BoxDown;
    }

    public void setBoxDown(int boxDown) {
        BoxDown = boxDown;
    }

    public int getImageResource() {
        return imageResource;
    }

    public void setImageResource(int imageResource) {
        this.imageResource = imageResource;
    }
}
