package com.mariocorp.miscontactos;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.os.Bundle;

import com.mariocorp.miscontactos.fragment.ContactsDetailsFragment;

public class PortraitDetailsContactActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_portrait_details_contact);

        Bundle bundle = getIntent().getExtras();
        if(bundle == null || bundle.getInt(Constans.POSITION, -1) == -1) {
            throw new RuntimeException("No se encontro posicion");
        }
        int posicion = bundle.getInt(Constans.POSITION);

        FragmentManager fm = getSupportFragmentManager();
        Fragment contactsDetailsFragment = fm.findFragmentById(R.id.fragment_holder_contat_details);
        if(contactsDetailsFragment == null) {
            FragmentTransaction transaction = fm.beginTransaction();
            contactsDetailsFragment = ContactsDetailsFragment.instantiate(posicion);
            transaction.replace(R.id.fragment_holder_contat_details, contactsDetailsFragment);
            //transaction.addToBackStack(null);
            transaction.commit();
        }

    }


}
