package com.mariocorp.miscontactos.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;

import com.mariocorp.miscontactos.R;
import com.mariocorp.miscontactos.model.SwipeFragmentProperties;

public class SwipeFragment extends Fragment {

    public static final String MESSAGE = "properties";

    public static SwipeFragment newInstance(SwipeFragmentProperties properties){
        SwipeFragment swipeFragment = new SwipeFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable(MESSAGE, properties);
        swipeFragment.setArguments(bundle);
        return swipeFragment;
    }

    @Nullable @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        SwipeFragmentProperties props = (SwipeFragmentProperties) getArguments().getSerializable(MESSAGE);
        View v = inflater.inflate(R.layout.fragment_swipe, container, false);
        ConstraintLayout root = v.findViewById(R.id.fragment_swipe_root);
        root.setBackgroundColor(props.getRootbackgroud());
        TextView txtMessage = v.findViewById(R.id.txt_fragment_message);
        txtMessage.setText(props.getMessage());
        ImageView imgUpBox = v.findViewById(R.id.img_box_up);
        imgUpBox.setBackgroundColor(props.getBoxUo());
        ImageView imgDownBox = v.findViewById(R.id.img_box_down);
        imgDownBox.setBackgroundColor(props.getBoxDown());
        ImageView imgRigth = v.findViewById(R.id.img_rigth);
        imgRigth.setImageResource(props.getImageResource());
        return v;
    }
}








