package com.mariocorp.miscontactos.model;

import java.io.Serializable;

public class Contact implements Serializable {

    private String mName;
    private String mAlias;
    private String mNumber;
    private String mEmail;

    public Contact(String mName, String mAlias, String mNumber, String mEmail) {
        this.mName = mName;
        this.mAlias = mAlias;
        this.mNumber = mNumber;
        this.mEmail = mEmail;
    }

    public String getName() {
        return mName;
    }

    public void setName(String mName) {
        this.mName = mName;
    }

    public String getAlias() {
        return mAlias;
    }

    public void setAlias(String mAlias) {
        this.mAlias = mAlias;
    }

    public String getNumber() {
        return mNumber;
    }

    public void setNumber(String mNumber) {
        this.mNumber = mNumber;
    }

    public String getEmail() {
        return mEmail;
    }

    public void setEmail(String mEmail) {
        this.mEmail = mEmail;
    }
}
