package com.mariocorp.miscontactos;

import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.mariocorp.miscontactos.adapter.ImagePagerAdapter;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import android.view.View;

public class SwipeActivity extends AppCompatActivity {
    ViewPager viewPager;
    PagerAdapter pagerAdapter;
    int[] images;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_swipe);
        images = new int[] { R.drawable.im1, R.drawable.im2, R.drawable.im3,
                             R.drawable.im4, R.drawable.im5, R.drawable.im6 };
        viewPager = findViewById(R.id.pager);
        pagerAdapter = new ImagePagerAdapter(SwipeActivity.this, images);
        viewPager.setAdapter(pagerAdapter);
    }

}
