package com.mariocorp.miscontactos.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.mariocorp.miscontactos.R;

public class ImagePagerAdapter extends PagerAdapter {

    private Context context;
    private int[] images;
    private LayoutInflater inflater;

    public ImagePagerAdapter(Context context, int[] images) {
        this.context = context;
        this.images = images;
    }

    @Override
    public int getCount() {//PARA QUE SEPA CUANTAS MOSTRAR
        return this.images.length;
    }

    @Override //POR SI ES EL MISMO QUE CREE UNO NUEVO SI NO REUTILIZAR
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view==object;//CASI SIEMPRE SERA ASI
    }


    @NonNull @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);//INFLADOR ESTANDART
        View itemView = inflater.inflate(R.layout.pager_item, container, false);
        ImageView currenImage = itemView.findViewById(R.id.page_item_image);
        currenImage.setImageResource(images[position]);
        ((ViewPager)container).addView(itemView);
        return itemView;
    }

    @Override//este nos da la pos y item=layout que ya se puede eliminar
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        ((ViewPager)container).removeView((LinearLayout) object);
    }
}
