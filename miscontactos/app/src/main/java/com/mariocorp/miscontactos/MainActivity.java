package com.mariocorp.miscontactos;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import com.mariocorp.miscontactos.coms.IActivityComs;
import com.mariocorp.miscontactos.fragment.ContactsDetailsFragment;
import com.mariocorp.miscontactos.fragment.ContactsListFragment;

public class MainActivity extends AppCompatActivity implements IActivityComs {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dual_main_activity);

        FragmentManager fm = getSupportFragmentManager();
        //si nos fijamos fragment_holder_contacts_list esta tanto en la vista hori como verti
        Fragment contactsListFragment = fm.findFragmentById(R.id.fragment_holder_contacts_list);
        if(contactsListFragment == null) {
            FragmentTransaction transaction = fm.beginTransaction();
            contactsListFragment = new ContactsListFragment();
            transaction.replace(R.id.fragment_holder_contacts_list, contactsListFragment);
            transaction.addToBackStack(null);
            transaction.commit();
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        Intent intent = null;
        if(item.getItemId() == R.id.menu_opt_swipe) {
            intent = new Intent(this, SwipeActivity.class);
        }

        if(item.getItemId() == R.id.menu_opt_swipe_fragment) {
            intent = new Intent(this, SwipeWithFragmetActivity.class);
        }

        if(intent != null){
            startActivity(intent);
            return true;
        }
        return false;
    }

    @Override
    public void onListItemSelected(int position) {
        if(findViewById(R.id.fragment_holder_contat_details) == null) {
            //ESTAMOS EN VERTICAL
            Intent intent = new Intent(this, PortraitDetailsContactActivity.class);
            intent.putExtra(Constans.POSITION, position);
            startActivity(intent);
        } else {
            //ESTAMOS EN HORIZONTAL
            FragmentManager fm = getSupportFragmentManager();
            FragmentTransaction transaction = fm.beginTransaction();

            Fragment fragmentContactDetailsOld = fm.findFragmentById(R.id.fragment_holder_contat_details); //recuperar el fragment por su ID
            Fragment fragmentContactDetailsNew = ContactsDetailsFragment.instantiate(position);

            if(fragmentContactDetailsOld != null) {
                //PUEDE DARSE EL CASO QUE YA SE MOSTRO UN DETALLE ANTERIOR ASI QUE AHI QUE SUSTITUIRLO
                transaction.remove(fragmentContactDetailsOld);
            }
            transaction.add(R.id.fragment_holder_contat_details, fragmentContactDetailsNew);
            transaction.commit();

        }
    }
}
