package com.mariocorp.databaseexample;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import androidx.annotation.Nullable;


public class DataManager {
    private static final String TAG = "DataManager";
    private SQLiteDatabase database;

    public static final String TABLE_ROW_ID = "_id";
    public static final String TABLE_ROW_NAME = "name";
    public static final String TABLE_ROW_AGE = "age";

    private static final String DATABASE_NAME = "my_db";
    private static final String TABLE_NAME = "persons";
    private static final int DATABASE_VERSION = 1;

    public DataManager(Context context) {
        CustomSQLiteopenHelper helper = new CustomSQLiteopenHelper(context, DATABASE_NAME, DATABASE_VERSION);
        database = helper.getWritableDatabase();//OBTENEMOS LA BD
    }

    public void insert(String name, String age) { //insertar
        String queryInsert = String.format(
         "INSERT INTO %s (%s, %s) VALUES('%s', '%s');",
         TABLE_NAME, TABLE_ROW_NAME, TABLE_ROW_AGE, name, age
        );
        Log.i(TAG, "insert query: " + queryInsert);
        database.execSQL(queryInsert);
    }

    public void delete(String name) { //eliminar
        String queryDelete = String.format(
                "DELETE FROM %s WHERE %s='%s';",
                TABLE_NAME, TABLE_ROW_NAME, name
        );
        Log.i(TAG, "delete query: " + queryDelete);
        database.execSQL(queryDelete);
    }

    public Cursor getUser(String name) { //obtener un usuario
        String queryGetUser = String.format(
                "SELECT %s, %s, %s FROM %s WHERE %s='%s';",
                TABLE_ROW_ID, TABLE_ROW_NAME, TABLE_ROW_AGE, TABLE_NAME, TABLE_ROW_NAME, name);
        Log.i(TAG, "getUser query: " + queryGetUser);
        Cursor c = database.rawQuery(queryGetUser, null);
        return c;
    }

    //obtener todos los usuarios
    public Cursor getAll() {
        String queryGetAll = String.format("SELECT * FROM %s;", TABLE_NAME);
        Log.i(TAG, "getAll query: " + queryGetAll);
        Cursor c = database.rawQuery(queryGetAll, null);
        return c;
    }



//####################################################
    private class CustomSQLiteopenHelper extends SQLiteOpenHelper {

        public CustomSQLiteopenHelper(@Nullable Context context, @Nullable String name, int version) {
            super(context, name, null, version);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            final String create_table_persons = String.format(
                "CREATE TABLE %s(" +
                        "%s integer primary key autoincrement not null, " +
                        "%s text not null, " +
                        "%s text not null" +
                 ");", TABLE_NAME, TABLE_ROW_ID, TABLE_ROW_NAME, TABLE_ROW_AGE
            );
            Log.i(TAG, "onCreate table persons: " + create_table_persons);
            //ERROR database.execSQL(create_table_persons); CUIDADO NO CONFUNDIR CON LA VARIABLE DEL DATAMANAGER
            db.execSQL(create_table_persons);
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        }
    }
}
