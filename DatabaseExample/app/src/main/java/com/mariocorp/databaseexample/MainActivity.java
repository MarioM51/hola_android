package com.mariocorp.databaseexample;

import androidx.appcompat.app.AppCompatActivity;

import android.database.Cursor;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String TAG = "MainActivity";
    Button btnInsert, btnDelete, btnSearch, btnAll;
    EditText inTxtNameToInsert, inTxtAgeToInsert, inTxtNameToDelete, inTxtNameToSearch;

    DataManager dataManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        dataManager = new DataManager(this);

        btnInsert = findViewById(R.id.btn_insert);
        btnDelete = findViewById(R.id.btn_delete);
        btnSearch = findViewById(R.id.btn_search);
        btnAll = findViewById(R.id.btn_select_all);
        btnInsert.setOnClickListener(this);
        btnDelete.setOnClickListener(this);
        btnSearch.setOnClickListener(this);
        btnAll.setOnClickListener(this);

        inTxtNameToInsert = findViewById(R.id.intxt_nombre);
        inTxtAgeToInsert = findViewById(R.id.intxt_edad);
        inTxtNameToDelete = findViewById(R.id.intxt_name_to_delete);
        inTxtNameToSearch = findViewById(R.id.intxt_name_to_seach);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_insert:
                dataManager.insert(inTxtNameToInsert.getText().toString(), inTxtAgeToInsert.getText().toString());
            break;
            case R.id.btn_delete:
                dataManager.delete(inTxtNameToDelete.getText().toString());
            break;
            case R.id.btn_search:
                Cursor cUser = dataManager.getUser(inTxtNameToSearch.getText().toString());
                showData(cUser);
            break;
            case R.id.btn_select_all:
                Cursor cAll = dataManager.getAll();
                showData(cAll);
            break;

            default:Log.d(TAG, "Callo en switch dafault"); break;
        }
    }


    //METODOS PARA MOSTRAR LA INFO

    private void showData(Cursor c) {
        String persontemp = null;
        while(c.moveToNext()) {
            persontemp = String.format("person: id: %s, name: %s, age: %s", c.getString(0), c.getString(1), c.getString(2));
            Log.i(TAG, "showData: " + persontemp);
        }
        Log.i(TAG, "showData: End");
    }


}
