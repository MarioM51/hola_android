package com.mariocorp.hellogps;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.core.location.LocationManagerCompat;

import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements LocationListener {

    private TextView txtLatitud, txtLogitud, txtSource;
    private LocationManager locationMng;
    private String provider;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        txtLatitud = findViewById(R.id.txt_lat);
        txtLogitud = findViewById(R.id.txt_long);
        txtSource = findViewById(R.id.txt_src);

        //hacemos referencia al servicio que ya tiene android
        locationMng = (LocationManager)getSystemService(Context.LOCATION_SERVICE);

        Criteria criteria = new Criteria();
        //el ultimo booleno indica si lo quremos siempre activo
        provider = locationMng.getBestProvider(criteria, false);

        Location location = null;
        boolean fineLocationGranted = ContextCompat.checkSelfPermission( getApplicationContext(), android.Manifest.permission.ACCESS_FINE_LOCATION ) != PackageManager.PERMISSION_GRANTED;
        boolean fineLocationCoarse = ContextCompat.checkSelfPermission( getApplicationContext(), android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED;
        if ( Build.VERSION.SDK_INT >= 23 && fineLocationGranted && fineLocationCoarse) {
            Toast.makeText(this, "Agrega permisos de acceso porfa", Toast.LENGTH_LONG);
        } else {
            location = locationMng.getLastKnownLocation(provider);
        }

        if(location != null) {
            txtSource.setText("Source: " + provider);
            onLocationChanged(location);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        locationMng.removeUpdates(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        //cada 1/2seg notificame o si el usr se mueve un metro notifica a esta clase
        boolean fineLocationGranted = ContextCompat.checkSelfPermission( getApplicationContext(), android.Manifest.permission.ACCESS_FINE_LOCATION ) != PackageManager.PERMISSION_GRANTED;
        boolean fineLocationCoarse = ContextCompat.checkSelfPermission( getApplicationContext(), android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED;
        if ( Build.VERSION.SDK_INT >= 23 && fineLocationGranted && fineLocationCoarse) {
            Toast.makeText(this, "Agrega permisos de acceso porfa", Toast.LENGTH_LONG);
        } else {
            locationMng.requestLocationUpdates(provider, 500, 1, this);
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        double lat = location.getLatitude();
        double longi = location.getLongitude();

        txtLatitud.setText("Latitud: " + String.valueOf(lat));
        txtLogitud.setText("Longitud: " + String.valueOf(longi));
        txtSource.setText("Source: " + provider);

    }

    @Override
    public void onStatusChanged(String inProvider, int status, Bundle extras) {
        txtSource.setText("Source: " + inProvider);
    }

    @Override
    public void onProviderEnabled(String inProvider) {
        txtSource.setText("Source: " + inProvider);
    }

    @Override
    public void onProviderDisabled(String inProvider) {
        txtSource.setText("Source: " + inProvider);
    }
}
