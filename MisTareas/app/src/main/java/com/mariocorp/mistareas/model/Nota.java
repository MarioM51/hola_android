package com.mariocorp.mistareas.model;

import org.json.JSONException;
import org.json.JSONObject;

public class Nota {
    private String mTitle;
    private String mDescripcion;
    private boolean mIdea;
    private boolean mNote;
    private boolean mImportant;

    private static final String JSON_TITLE = "title";
    private static final String JSON_DESCRIPTION = "description";
    private static final String JSON_IDEA = "idea";
    private static final String JSON_TODO = "todo";
    private static final String JSON_IMPORTANT = "important";

    public Nota() { }

    public Nota(JSONObject jo) throws JSONException {
        mTitle = jo.getString(JSON_TITLE);
        mDescripcion = jo.getString(JSON_DESCRIPTION);

        mIdea = jo.getBoolean(JSON_IDEA);
        mImportant = jo.getBoolean(JSON_IMPORTANT);
        mNote = jo.getBoolean(JSON_TODO );
    }

    public JSONObject convertNoteToJSON() throws JSONException {
        JSONObject jo = new JSONObject();
        jo.put(JSON_TITLE, mTitle);
        jo.put(JSON_DESCRIPTION, mDescripcion);
        jo.put(JSON_IDEA, mIdea);
        jo.put(JSON_IMPORTANT, mImportant);
        jo.put(JSON_TODO, mNote);
        return jo;
    }

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String mTitle) {
        this.mTitle = mTitle;
    }

    public String getDescripcion() {
        return mDescripcion;
    }

    public void setDescripcion(String mDescripcion) {
        this.mDescripcion = mDescripcion;
    }

    public boolean isIdea() {
        return mIdea;
    }

    public void setIdea(boolean mIdea) {
        this.mIdea = mIdea;
    }

    public boolean isNote() {
        return mNote;
    }

    public void setNote(boolean mNote) {
        this.mNote = mNote;
    }

    public boolean isImportant() {
        return mImportant;
    }

    public void setImportant(boolean mImportant) {
        this.mImportant = mImportant;
    }
}
