package com.mariocorp.mistareas.dialog;

import android.app.Dialog;
import android.app.Notification;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.core.view.LayoutInflaterCompat;
import androidx.fragment.app.DialogFragment;

import com.mariocorp.mistareas.MainActivity;
import com.mariocorp.mistareas.R;
import com.mariocorp.mistareas.model.Nota;

public class DialogNewNote extends DialogFragment {

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        //creamos el constructor de la vista y la vista con el layout de nueva nota
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.layout_new_note, null);

        //tomamos el formulario para poder tomar sus datos
        final EditText editTitle = dialogView.findViewById(R.id.txtv_title);
        final EditText editDescription = dialogView.findViewById(R.id.txtv_descripcion);
        final CheckBox checkBoxIdea = dialogView.findViewById(R.id.chbx_idea);
        final CheckBox checkBoxTodo = dialogView.findViewById(R.id.chbx_todo);
        final CheckBox checkBoxImportant = dialogView.findViewById(R.id.chbx_important);

        //boton crear
        Button btnOk = dialogView.findViewById(R.id.btn_new_note_ok);
        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //creamos la nota con los datos del formulario
                Nota newNote = new Nota();
                newNote.setTitle(editTitle.getText().toString());
                newNote.setDescripcion(editDescription.getText().toString());
                newNote.setIdea(checkBoxIdea.isChecked());
                newNote.setNote(checkBoxTodo.isChecked());
                newNote.setImportant(checkBoxImportant.isChecked());

                //le mandamos la nota a mainActivity para que la muestre
                MainActivity mainActivity = (MainActivity)getActivity();
                mainActivity.createNewNote(newNote);
                dismiss();//cerramos el dialogo
            }
        });
        //boton cancelar
        Button btnCancel = dialogView.findViewById(R.id.btn_new_note_cancel);
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) { dismiss(); }
        });
        //ponemos titulo al dialog
        builder.setView(dialogView).setMessage("Añadir una nueva nota");
        return builder.create();
    }
}
