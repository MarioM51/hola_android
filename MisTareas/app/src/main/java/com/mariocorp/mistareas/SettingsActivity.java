package com.mariocorp.mistareas;

import android.content.SharedPreferences;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;

public class SettingsActivity extends AppCompatActivity {
    private SharedPreferences mPrefs;
    private SharedPreferences.Editor mPrefsEditor;
    private boolean mSound;
    public static final byte FAST = 2, SLOW = 1, NONE = 0;
    public static byte mAnimationPrefOptionSelected;

    final public static String preferenceSoundName = "sound";
    final public static String preferenceAnimationName = "speed_animation";
    final public static String nameSettingsSaved = "todo_app_settings";

    @Override protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        mPrefs = getSharedPreferences("todo_app_settings", MODE_PRIVATE);
        mPrefsEditor = mPrefs.edit();

        //CAMBIO Y GUARDADO DE PREFERENCIA DE SONIDO
        this.mSound = mPrefs.getBoolean(preferenceSoundName, true);
        final CheckBox chBoxSound = findViewById(R.id.chb_sound);
        chBoxSound.setChecked(mSound);
        chBoxSound.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                chBoxSound.setChecked(!mSound);
                mPrefsEditor.putBoolean(preferenceSoundName, !mSound);
                //mPrefsEditor.commit(); // lo pasamos al onPause para que sea mas eficiente
            }
        });

        //CAMBIO Y GUARDADO DE PREFERENCIA DE VELOCIDAD DE ANIMACION
        this.mAnimationPrefOptionSelected = (byte)mPrefs.getInt(preferenceAnimationName, FAST);
        final RadioGroup rdGroup = findViewById(R.id.rb_group_animation);
        rdGroup.clearCheck(); //deseleccionamos todos
        switch (mAnimationPrefOptionSelected) {
            case(FAST): rdGroup.check(R.id.rb_sound_fast); break;
            case(SLOW): rdGroup.check(R.id.rb_sound_slow); break;
            case(NONE): rdGroup.check(R.id.rb_sound_none); break;
        }
        rdGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override public void onCheckedChanged(RadioGroup group, int checkedId) {
                RadioButton rbSelected = rdGroup.findViewById(checkedId); //RECUPERAMOS EL RADIO NUTON SELECCIONADO
                if(null != rbSelected) {
                    switch (rbSelected.getId()) {
                        case(R.id.rb_sound_fast): mAnimationPrefOptionSelected = FAST;  break;
                        case(R.id.rb_sound_slow):  mAnimationPrefOptionSelected = SLOW;  break;
                        case(R.id.rb_sound_none): mAnimationPrefOptionSelected = NONE;  break;
                    }
                }
                mPrefsEditor.putInt(preferenceAnimationName, mAnimationPrefOptionSelected);
                //mPrefsEditor.commit(); // lo pasamos al onPause para que sea mas eficiente
            }
        });
    }

    @Override protected void onPause() {
        super.onPause();
        mPrefsEditor.commit();
    }
}
