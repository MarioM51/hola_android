package com.mariocorp.mistareas;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.mariocorp.mistareas.dialog.DialogNewNote;
import com.mariocorp.mistareas.dialog.DialogShowNote;
import com.mariocorp.mistareas.model.Nota;
import com.mariocorp.mistareas.utils.JSONSerializer;

import org.json.JSONException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements Animation.AnimationListener {

    private NoteAdapter mNoteAdapter;

    private SharedPreferences mPrefs;

    private Animation mAnimFlash;
    private Animation mAnimFadeIn;
    private int mPrefSpeedAnimation;
    boolean mPrefSoundActivated;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.mNoteAdapter = new NoteAdapter();
        ListView listNotes = findViewById(R.id.list_todo);
        listNotes.setAdapter(this.mNoteAdapter);            //asigansmo el adapter a la lista
        //Agregamos un click listener a cada item de la lista
        listNotes.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override public void onItemClick(AdapterView<?> parent, View view, int posPulsada, long id) {
                //mostramos detalle de la nota
                Nota notaTemp = (Nota)mNoteAdapter.getItem(posPulsada);
                DialogShowNote dialogShowNote = new DialogShowNote();
                Log.d("Main", "Mostrando Nota");
                dialogShowNote.showNote(notaTemp);
                dialogShowNote.show(getSupportFragmentManager(), "show_note");
            }
        });

        listNotes.setLongClickable(true);
        listNotes.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                mNoteAdapter.removeNote(position);
                return true;
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        mPrefs = getSharedPreferences(SettingsActivity.nameSettingsSaved, MODE_PRIVATE);
        this.mPrefSpeedAnimation = mPrefs.getInt(SettingsActivity.preferenceAnimationName, SettingsActivity.FAST);
        this.mPrefSoundActivated = mPrefs.getBoolean(SettingsActivity.preferenceSoundName, true);
        Log.i("Main", String.format("Sonido: %s - Velocidad Animacion: %s", mPrefSoundActivated, mPrefSpeedAnimation));

        this.mAnimFadeIn = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fade_in);
        this.mAnimFlash = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.flash_notas);

        if(mPrefSpeedAnimation == SettingsActivity.FAST) {
            mAnimFlash.setDuration(200);
        } else if(mPrefSpeedAnimation == SettingsActivity.SLOW) {
            mAnimFlash.setDuration(1000);
        }
        mNoteAdapter.notifyDataSetChanged();
    }

    public void createNewNote(Nota newNote) {               //Agregamos la nueva nota a la lista
        mNoteAdapter.addNote(newNote);
    }

    @Override public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_name, menu);
        return super.onCreateOptionsMenu(menu);
    }
    @Override public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if(item.getItemId() == R.id.menu_opt_add) {
            DialogNewNote dialogNewNote = new DialogNewNote();
            dialogNewNote.show(getSupportFragmentManager(), "add_note");
            return true;
        }

        if(item.getItemId() == R.id.menu_opt_settings) {
            Intent intent = new Intent(this, SettingsActivity.class);
            startActivity(intent);
            return true;
        }

        if(item.getItemId() == R.id.menu_opt_animations) {
            Intent intent = new Intent(this, AnimActivity.class);
            startActivity(intent);
            return true;
        }

        if(item.getItemId() == R.id.menu_opt_sounds) {
            Intent intent = new Intent(this, SoundsActivity.class);
            startActivity(intent);
            return true;
        }

        if(item.getItemId() == R.id.menu_opt_myDevice) {
            Intent intent = new Intent(this, DetectDiviceActivity.class);
            startActivity(intent);
            return true;
        }

        if(item.getItemId() == R.id.menu_opt_fragments) {
            Intent intent = new Intent(this, FragmentosActivity.class);
            startActivity(intent);
            return true;
        }

        if(item.getItemId() == R.id.menu_opt_recycler_view) {
            Intent intent = new Intent(this, MyRecyclerViewActivity.class);
            startActivity(intent);
            return true;
        }
        return false;
    }

    @Override
    protected void onPause() {
        super.onPause();
        mNoteAdapter.saveNotas();
    }

    @Override
    public void onAnimationStart(Animation animation) {
        Log.d("Main", "Inicio la animacion");
    }

    @Override
    public void onAnimationEnd(Animation animation) {
        Log.d("Main", "Fin animacion");
    }

    @Override
    public void onAnimationRepeat(Animation animation) {
        Log.d("Main", "Repetio");
    }

    //----------------
    public class NoteAdapter extends BaseAdapter {
        private JSONSerializer mJsonSerializer;

        List<Nota> notes = new ArrayList<Nota>();

        public NoteAdapter() {
            mJsonSerializer = new JSONSerializer("tareas_app.json", MainActivity.this.getApplicationContext());
            try { notes = mJsonSerializer.load(); }
            catch (Exception e) { e.printStackTrace(); }
        }

        public void saveNotas() {
            try {
                mJsonSerializer.save(notes);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override public int getCount() { return notes.size(); }
        @Override public Object getItem(int position) { return notes.get(position); }
        @Override public long getItemId(int position) { return position; }

        @Override public View getView(int position, View convertView, ViewGroup parent) {
            if(convertView == null) {
                //la lista aun no ha sido inflada a si que lo primero es inflarla a partir del layout (.xml)
                LayoutInflater inflater = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = inflater.inflate(R.layout.note_item_list, parent, false);
            }
            //ya tenemos la vista creada ahora la seteamos
            TextView txtTitle = convertView.findViewById(R.id.txt_title);
            TextView txtDescription = convertView.findViewById(R.id.txt_description);
            ImageView imgImportant = convertView.findViewById(R.id.img_important);
            ImageView imgIdea = convertView.findViewById(R.id.img_idea);
            ImageView imgNote = convertView.findViewById(R.id.img_nota);
            Nota notaActual = notes.get(position);
            txtTitle.setText(notaActual.getTitle());
            txtDescription.setText(notaActual.getDescripcion());
            //APLICAMOS ANIMACIONES
            if(notaActual.isImportant() && mPrefSpeedAnimation != SettingsActivity.NONE ) {
                convertView.setAnimation(mAnimFlash);
            } else {
                convertView.setAnimation(mAnimFadeIn);
            }

            if(!notaActual.isIdea()) { imgIdea.setVisibility(View.GONE); }
            if(!notaActual.isImportant()) { imgImportant.setVisibility(View.GONE); }
            if(!notaActual.isNote()) { imgNote.setVisibility(View.GONE); }

            return convertView;
        }
        public void addNote(Nota notaIn) {
            notes.add(notaIn);
            notifyDataSetChanged();  //para que pinte los cambios cuando agregemos una nota
        }

        public void removeNote(int n) {
            notes.remove(n);
            notifyDataSetChanged();
        }
    }
}
