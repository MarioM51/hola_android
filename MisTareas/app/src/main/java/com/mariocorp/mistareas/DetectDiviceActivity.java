package com.mariocorp.mistareas;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Point;
import android.os.Bundle;
import android.view.Display;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class DetectDiviceActivity extends AppCompatActivity {

    private TextView txtResolution;
    private TextView txtorientation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detect_divice);

        txtResolution = findViewById(R.id.txt_resolucion);
        txtorientation = findViewById(R.id.txt_orientacion);

        Button btnDetectar = findViewById(R.id.btn_detectar);
        btnDetectar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                detectDevice();
            }
        });

    }

    private void detectDevice() {
        Display display = getWindowManager().getDefaultDisplay();
        txtorientation.setText(display.getRotation()+"");
        Point xy = new Point();
        display.getSize(xy);
        txtResolution.setText(String.format("X: %s Y: %s", xy.x+"", xy.y+""));
    }
}
