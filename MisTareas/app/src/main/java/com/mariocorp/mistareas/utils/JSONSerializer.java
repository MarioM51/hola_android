package com.mariocorp.mistareas.utils;

import android.content.Context;

import com.mariocorp.mistareas.model.Nota;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONTokener;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;

public class JSONSerializer {
    private final String FILE_NAME;
    private final Context mContext; //para saber donde se guardara el file json

    public JSONSerializer(String InFILE_NAME, Context InmContext) {
        FILE_NAME = InFILE_NAME;
        mContext = InmContext;
    }

    public void save(List<Nota> notas) throws IOException, JSONException {
        JSONArray jsonArray = new JSONArray();
        for(Nota n: notas){ jsonArray.put(n.convertNoteToJSON()); }
        Writer writer = null;
        try {
            OutputStream out = mContext.openFileOutput(FILE_NAME, mContext.MODE_PRIVATE);//ABRIMOS EL ARCHIVO
            writer = new OutputStreamWriter(out);            //CREAMOS EL ESCRITOR DEL ARCHIVO
            writer.write(jsonArray.toString());                     //PASAMOS A STRING EL ARRAY Y LO ESCRIBIMOS EN DISCO
        } finally {
            if (null != writer) { writer.close(); }
        }
    }

    public ArrayList<Nota> load() throws IOException, JSONException  {
        ArrayList<Nota> notas = new ArrayList<Nota>();
        BufferedReader bufferedReader = null;
        try {
            InputStream in = mContext.openFileInput(FILE_NAME);
            bufferedReader = new BufferedReader(new InputStreamReader(in));

            //OBTENEMOS DEL FILE ALL THE STRING CONTAINED IN THERE
            StringBuilder jsonString = new StringBuilder();
            String currentLine = null;
            while ((currentLine=bufferedReader.readLine()) != null) { jsonString.append(currentLine); }

            //DEL STRING LO PASAMOS A OBJETO JSON Y LO PASAMOS A UNA LISTA JAVA DE NOTAS
            JSONArray jsonArray = (JSONArray)new JSONTokener(jsonString.toString()).nextValue();
            for (int i=0; i<jsonArray.length(); i++) { notas.add(new Nota(jsonArray.getJSONObject(i))); }
        } catch(FileNotFoundException e) {
            //LA PRIMERA VEZ NO VA A HAVER FIVHERO LO POEMOS IGNORAR YA QUE SIGNIFICA QUE NO SE A CREADO NINGUNA NOTA
        } finally {
            if (bufferedReader != null) { bufferedReader.close(); }
        }
        return notas;
    }

}
