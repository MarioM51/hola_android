package com.mariocorp.mistareas.model;

public class Producto {
    private String nombre;
    private String urlPhoto;
    private float precio;
    private int valoracion;

    public Producto(String nombre, String urlPhoto, float precio, int valoracion) {
        this.nombre = nombre;
        this.urlPhoto = urlPhoto;
        this.precio = precio;
        this.valoracion = valoracion;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getUrlPhoto() {
        return urlPhoto;
    }

    public void setUrlPhoto(String urlPhoto) {
        this.urlPhoto = urlPhoto;
    }

    public float getPrecio() {
        return precio;
    }

    public void setPrecio(float precio) {
        this.precio = precio;
    }

    public int getValoracion() {
        return valoracion;
    }

    public void setValoracion(int valoracion) {
        this.valoracion = valoracion;
    }
}
