package com.mariocorp.mistareas.adapter;

import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.mariocorp.mistareas.fragment.ProductoFragment.OnListFragmentInteractionListener;
import com.mariocorp.mistareas.R;
import com.mariocorp.mistareas.model.Producto;
import com.squareup.picasso.Picasso;

import java.util.List;

public class MyProductoRecyclerViewAdapter extends RecyclerView.Adapter<MyProductoRecyclerViewAdapter.ViewHolder> {

    private final List<Producto> mProductos;
    private final OnListFragmentInteractionListener mListener;

    public MyProductoRecyclerViewAdapter(List<Producto> items, OnListFragmentInteractionListener listener) {
        mProductos = items;
        mListener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.fragment_producto, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mProducto = mProductos.get(position);

        holder.mNombre.setText(holder.mProducto.getNombre());
        holder.mPrecio.setText(String.valueOf(holder.mProducto.getPrecio()));
        Picasso.get()
                .load("https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQmeCrtkoFuGHkWx4a28zHzFt1HjMZvN7xHrY1CYohR-hmPMQaq&s")
                .resize(400, 100)
                .centerCrop()
                .into(holder.mFoto);
        holder.mValoracion.setRating(holder.mProducto.getValoracion());

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    mListener.onListFragmentInteraction(holder.mProducto);
                }
            }
        });
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView mNombre;
        public final TextView mPrecio;
        public final ImageView mFoto;
        public final RatingBar mValoracion;

        public Producto mProducto;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            mNombre = mView.findViewById(R.id.item_list_poducto_nombre);
            mPrecio = mView.findViewById(R.id.item_list_poducto_precio);
            mFoto = mView.findViewById(R.id.item_list_poducto_img);
            mValoracion = mView.findViewById(R.id.item_list_poducto_valoracion);
        }
    }

    @Override
    public int getItemCount() {
        return mProductos.size();
    }
}
