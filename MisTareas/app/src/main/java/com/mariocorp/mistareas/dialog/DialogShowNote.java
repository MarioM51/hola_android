package com.mariocorp.mistareas.dialog;

import android.app.Dialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.DialogFragment;

import com.mariocorp.mistareas.MainActivity;
import com.mariocorp.mistareas.R;
import com.mariocorp.mistareas.model.Nota;

public class DialogShowNote extends DialogFragment {

    private Nota mNote;

    public void showNote(Nota inNote) {
        this.mNote = inNote;
    }

    @NonNull @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        //creamos el constructor de la vista y la vista con el layout de nueva nota
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.layout_show_note, null);

        //tomamos el formulario para poder tomar sus datos
        final TextView txtViewTitle = dialogView.findViewById(R.id.txtv_title);
        final TextView txtViewDescription = dialogView.findViewById(R.id.txtv_description);

        txtViewTitle.setText(this.mNote.getTitle());
        txtViewDescription.setText(this.mNote.getDescripcion());

        ImageView imgImportant = dialogView.findViewById(R.id.img_important);
        ImageView imgTodo = dialogView.findViewById(R.id.img_check);
        ImageView imgideea = dialogView.findViewById(R.id.img_idea);

        if(!this.mNote.isIdea()) {
            imgideea.setVisibility(View.GONE);
        }

        if(!this.mNote.isImportant()) {
            imgImportant.setVisibility(View.GONE);
        }

        if(!this.mNote.isNote()) {
            imgTodo.setVisibility(View.GONE);
        }

        //boton cancelar
        Button btnok = dialogView.findViewById(R.id.btn_show_note_ok);
        btnok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) { dismiss(); }
        });
        //ponemos titulo al dialog
        builder.setView(dialogView).setMessage("Detalles nota");
        return builder.create();
    }
}
