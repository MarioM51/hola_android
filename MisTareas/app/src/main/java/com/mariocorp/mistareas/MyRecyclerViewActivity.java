package com.mariocorp.mistareas;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.mariocorp.mistareas.fragment.ProductoFragment;
import com.mariocorp.mistareas.model.Producto;

public class MyRecyclerViewActivity extends AppCompatActivity implements ProductoFragment.OnListFragmentInteractionListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_recycler_view);
    }

    @Override
    public void onListFragmentInteraction(Producto item) {

    }
}
