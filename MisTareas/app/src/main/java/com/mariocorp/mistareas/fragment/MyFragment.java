package com.mariocorp.mistareas.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.mariocorp.mistareas.R;

public class MyFragment extends androidx.fragment.app.Fragment {
    private String mAlgunString; private Button btnMybutton;

    @Override public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mAlgunString = "Texto dentro de nun fragmento";
    }

    @Override @Nullable
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.my_fragment_layout, container, false);
        btnMybutton = v.findViewById(R.id.btn_dentro_fragmento);
        btnMybutton.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) { Toast.makeText(getActivity(), "hola desde Fragment", Toast.LENGTH_SHORT).show(); }
        });
        return v;
    }
}
