package com.mariocorp.snapmap;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import com.mariocorp.snapmap.comunication.ActivityComs;
import com.mariocorp.snapmap.fragment.CaptureFragment;
import com.mariocorp.snapmap.fragment.ShowPhotoFragment;
import com.mariocorp.snapmap.fragment.TagsFragment;
import com.mariocorp.snapmap.fragment.TitlesFragment;
import com.mariocorp.snapmap.repository.DataManager;

public class MainActivity extends AppCompatActivity implements ActivityComs {
    private static final String TAG = "MainActivity";

    private ListView mNavDrawerListView;
    private DrawerLayout mDrawerLayout;
    private ArrayAdapter<String> mArrayAdapter;
    private ActionBarDrawerToggle mActionBarDrawerToggle;
    private String mActivityTitle;

    DataManager dataManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        dataManager = new DataManager(getApplicationContext());

        mNavDrawerListView = findViewById(R.id.nav_list);
        mDrawerLayout = findViewById(R.id.drawer_layout);
        mActivityTitle = getTitle().toString();

        //CARGAMOS LOS ITEMS/OPCIONES EN EL MENU
        String[] navmenuTitles = getResources().getStringArray(R.array.nav_drawer_options);
        mArrayAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, navmenuTitles);
        mNavDrawerListView.setAdapter(mArrayAdapter);

        this.setupDrawer();//configuramos el boton

        //habilitamos el control del actionbar donde configuramos el boton
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        mNavDrawerListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                switchFragment(position);
            }
        });

        switchFragment(0); // CARGAMOS LA PANTALLA INICIAL
    }
    private void switchFragment(int position) {
        Fragment fragment = null;
        String fragmentID = null;
        switch (position) { //decidimos que Fragment mostrar
            case 0:
                fragmentID = "TITLES";
                Bundle bundle = new Bundle();//nos servira para pasarle tags
                bundle.putString(ActivityComs.TAG_NAME, "NO_TAG");
                fragment = new TitlesFragment();
                fragment.setArguments(bundle);
            break;
            case 1:
                fragmentID = "TAGS";
                fragment = new TagsFragment();
            break;
            case 2:
                fragmentID = "CAPTURE";
                fragment = new CaptureFragment();
            break;
            default:
                Log.e(TAG, "Entro en el switch default que no deberia de entrar, Posicion: " + position);
            break;
        }
        //mostramos fragment
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.fragment_holder, fragment, fragmentID)
                .commit();
        //cerramos menu de navegacion
        mDrawerLayout.closeDrawer(mNavDrawerListView);
    }

    private void setupDrawer() {//creamos el boton para abrir y cerrar el nav menu
        mActionBarDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout,
                R.string.drawer_open, R.string.drawer_close){
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                getSupportActionBar().setTitle(getString(R.string.menu_choice));//CAMBIARA EL TEXTO DE ARRIBA
                invalidateOptionsMenu();//LLAMA AUTO. AL METODO onPrepareOptionMenu PARA QUE REFRESQUE EL TEXTO
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                getSupportActionBar().setTitle(mActivityTitle);
                invalidateOptionsMenu();
            }
        };
        mActionBarDrawerToggle.setDrawerIndicatorEnabled(true);//LO ACTIVAMOS
        //@deprecated: mDrawerLayout.setDrawerListener(mActionBarDrawerToggle);
        mDrawerLayout.addDrawerListener(mActionBarDrawerToggle);
    }

    @Override
    public void onPostCreate(@Nullable Bundle savedInstanceState, @Nullable PersistableBundle persistentState) {
        super.onPostCreate(savedInstanceState, persistentState);
        mActionBarDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mActionBarDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed(); si no lo quitamos hace lo dicho por default
        if(mDrawerLayout.isDrawerOpen(GravityCompat.START)) { // el nav menu esta abierto

            mDrawerLayout.closeDrawer(mNavDrawerListView);
        } else {// el nav menu esta cerrado
            Fragment currentFragment = getSupportFragmentManager().findFragmentById(R.id.fragment_holder);
            if(currentFragment instanceof TitlesFragment) {//ESTOY EN TITLE DEBO CERRAR LA APP
                finish();
                System.exit(0);//TODO: DEBERIAMOS DE PREGUNTAR ANTES DE SALIR
            } else { //NO ESTOY EN TITLE (PANTALLA INICIAL) DEBO IR A ELLA
                this.switchFragment(0);
            }
        }
    }
    // WHAT
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();
        if(mActionBarDrawerToggle.onOptionsItemSelected(item)){
            return true;
        }

        if(id == R.id.menu_opt_settings) {
            Toast.makeText(this, "Se selecciono settings", Toast.LENGTH_SHORT).show();
        }

        return super.onOptionsItemSelected(item);
    }
    // */

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }

    //ESTE METODO SE LLAMARA CUANDO SELECCIONEMOS UN TITULO
    @Override public void onTitleListItemSelected(int idPhoto) {
        Bundle bundle = new Bundle();
        bundle.putInt(ActivityComs.ID_PHOTO, idPhoto);

        ShowPhotoFragment showPhotoFragment = new ShowPhotoFragment();
        showPhotoFragment.setArguments(bundle);

        if(showPhotoFragment != null) {
            // si da nulo significa que talvez ocurrio un error,
            // lo mas seguro por la carga de la foto
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction()
                    .replace(R.id.fragment_holder, showPhotoFragment, "SHOW_PHOTO")
                    .commit();

            //ACTUALIZACMO EL TITULO Y LA OPCION SELECCIONADA, EN ESTE CASO
            mNavDrawerListView.setItemChecked(1, true);
            mNavDrawerListView.setSelection(1);
            mDrawerLayout.closeDrawer(mNavDrawerListView);//POR SI ESTABA AVIERTO EL MENU LO CIERRO

        }

    }

    //ESTE METODO SE LLAMARA CUANDO SELECCIONEMOS UN TAG
    @Override public void onTagListItemSelected(String tagToSearch) {
        Bundle bundle = new Bundle();
        bundle.putString(ActivityComs.TAG_NAME, tagToSearch);

        TitlesFragment titlesFragment = new TitlesFragment();
        titlesFragment.setArguments(bundle);

        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.fragment_holder, titlesFragment, "TAGS")
                .commit();

        //ACTUALIZACMO EL TITULO Y LA OPCION SELECCIONADA, EN ESTE CASO
        mNavDrawerListView.setItemChecked(1, true);
        mNavDrawerListView.setSelection(1);
        mDrawerLayout.closeDrawer(mNavDrawerListView);//POR SI ESTABA AVIERTO EL MENU LO CIERRO
    }
}
