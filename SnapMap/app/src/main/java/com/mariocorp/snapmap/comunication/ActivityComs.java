package com.mariocorp.snapmap.comunication;

public interface ActivityComs {
    String ID_PHOTO = "idPhoto";
    String TAG_NAME = "tag";

    //se llamara cuando se precione sobre un titulo de la lista de titulos
    void onTitleListItemSelected(int idPhoto);
    //se llamara cuando se precione sobre un TAG de la lista de tags
    void onTagListItemSelected(String tagToSearch);
}
