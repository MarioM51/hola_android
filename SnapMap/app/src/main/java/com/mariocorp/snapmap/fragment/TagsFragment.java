package com.mariocorp.snapmap.fragment;

import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.ListFragment;

import com.mariocorp.snapmap.R;
import com.mariocorp.snapmap.comunication.ActivityComs;
import com.mariocorp.snapmap.repository.DataManager;

public class TagsFragment extends ListFragment {
    public static final String TAG = "TagsFragment";

    private Cursor mCursor;
    private ActivityComs mActivityComs;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        DataManager dataManager = new DataManager(getActivity().getApplicationContext());

        mCursor = dataManager.getTags();

        SimpleCursorAdapter simpleCursorAdapter = new SimpleCursorAdapter(
                getActivity(),//contexto
                android.R.layout.simple_list_item_1,
                mCursor,                  //la primera posicion de nuestra coleccion de datos
                new String[]{DataManager.TABLE_ROW_TAG}, //QUE DATO VAMOS A MOSTRAR
                new int[]{android.R.id.text1},           //TEXTO BASICO
                0
        );
        setListAdapter(simpleCursorAdapter);
    }

    @Override
    public void onListItemClick(ListView list, View v, int position, long id) {
        //CUAL A SIDO LA ETIQUETA QUE SE A SELECCIONADO
        Cursor c = ((SimpleCursorAdapter)list.getAdapter()).getCursor();//obtengo todos
        c.moveToPosition(position); //apunto a la laseleccionada
        String tag = c.getString(c.getColumnIndex(DataManager.TABLE_ROW_TAG));
        Log.i(TAG, "Etiqueta seleccionada: " + tag);

        //ENVIAMOS TAG A MAIN_ACTIVITY
        mActivityComs.onTagListItemSelected(tag);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mActivityComs = (ActivityComs)context;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mActivityComs = null;
    }

}
