package com.mariocorp.snapmap.fragment;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.mariocorp.snapmap.R;
import com.mariocorp.snapmap.comunication.ActivityComs;
import com.mariocorp.snapmap.repository.DataManager;

import java.util.Locale;

public class ShowPhotoFragment extends Fragment {
    private Cursor mCursor;
    private ImageView mImg;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        int idPhoto = getArguments().getInt(ActivityComs.ID_PHOTO, -1);
        if(idPhoto == -1) { Toast.makeText(getActivity(), "ERROR, no se encontro posicion", Toast.LENGTH_LONG).show(); }
        DataManager dataManager = new DataManager(getActivity().getApplicationContext());
        mCursor = dataManager.getPhoto(idPhoto);
    }

    @Nullable @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragmet_show_photo, container, false);
        TextView txtTitle = v.findViewById(R.id.show_photo_txt_title);
        mImg = v.findViewById(R.id.show_photo_img);
        Button btnShowMap = v.findViewById(R.id.show_photo_btn_show_map);
        btnShowMap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //SACAMOS DE LA BD LA POSICION DE LA FOTO QUE ESTAMOS MOSTRANDO
                double latitude = Double.valueOf(mCursor.getString(mCursor.getColumnIndex(DataManager.TABLE_ROW_LOCATION_LAT)));
                double longitud = Double.valueOf(mCursor.getString(mCursor.getColumnIndex(DataManager.TABLE_ROW_LOCATION_LONG)));

                //LANZAMOS INTENT Y LE PASAMOS LaT Y LONG POT URI
                String uri = String.format(Locale.ENGLISH, "geo:%f,%f", latitude, longitud);
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
                getActivity().startActivity(intent);

            }
        });

        txtTitle.setText(mCursor.getString(mCursor.getColumnIndex(DataManager.TABLE_ROW_TITLE)));
        mImg.setImageURI(Uri.parse(mCursor.getString(mCursor.getColumnIndex(DataManager.TABLE_ROW_URI))));

        return v;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        //BitmapDrawable bmd = (BitmapDrawable) mImg.getDrawable();
        //bmd.getBitmap().recycle();
        //mImg.setImageBitmap(null);
    }
}
