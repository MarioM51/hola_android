package com.mariocorp.snapmap.fragment;

import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.ListFragment;

import com.mariocorp.snapmap.R;
import com.mariocorp.snapmap.comunication.ActivityComs;
import com.mariocorp.snapmap.repository.DataManager;

public class TitlesFragment extends ListFragment {
    private Cursor mCursor;
    private ActivityComs mActivityComs;

    private static final String TAG = "TitlesFragment";

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        String tag = getArguments().getString(ActivityComs.TAG_NAME, null);
        if(tag==null) {
            Log.e(TAG, "Chin Chin no llego el TAG");
        }

        DataManager dataManager = new DataManager(getActivity().getApplicationContext());
        if(tag == "NO_TAG"){
            //si el usuario no filtro por ninguna tag significa que quiere ver todas las fotos
            mCursor = dataManager.getTitles();
        } else {
            //si ahi un tag el usr solo quiere las fotos con dicho tag.
            mCursor = dataManager.getTitlesWithTag(tag);
        }

        //ESTE ADAPTER HARA EL TRABAJO SUCIO CON EL CURSOR
        SimpleCursorAdapter simpleCursorAdapter = new SimpleCursorAdapter(
            getActivity(),//contexto
            android.R.layout.simple_list_item_1,
            mCursor,                  //la primera posicion de nuestra coleccion de datos
            new String[]{DataManager.TABLE_ROW_TITLE}, //QUE DATO VAMOS A MOSTRAR
            new int[]{android.R.id.text1},
            0
        );
        setListAdapter(simpleCursorAdapter);

    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        mCursor.moveToPosition(position);//vamos a la posicion de la lista seleccionada
        // del seleccionado extramemos su id
        int idPhoto = mCursor.getInt(mCursor.getColumnIndex(DataManager.TABLE_ROW_ID));

        //ENVIAMOS ID DEL TELEFONO A MAIN_ACTIVITY
        mActivityComs.onTitleListItemSelected(idPhoto);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mActivityComs = (ActivityComs)context;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mActivityComs = null;
    }
}
