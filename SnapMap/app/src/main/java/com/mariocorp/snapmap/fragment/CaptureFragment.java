package com.mariocorp.snapmap.fragment;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import com.mariocorp.snapmap.R;
import com.mariocorp.snapmap.model.Photo;
import com.mariocorp.snapmap.repository.DataManager;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class CaptureFragment extends Fragment implements LocationListener {

    private static final int CAMERA_REQUEST = 12345;
    private static final int GEOLACATION_REQUEST_PERMISION = 1382;

    private ImageView mImgFot;
    private Uri mImageUri = Uri.EMPTY;
    private String mCurrentPhotoPath;
    private Button btnTakePhoto, btnSavePhoto, btnCancel;

    private Location mLocation;
    private LocationManager mLocationManager;
    private String mProvider;
    Criteria mCriteria;

    private DataManager mDataManager; //lo inicializamos en onCreate

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mDataManager = new DataManager(getActivity().getApplicationContext());

        //para la captura de la localizacion el el metodo onLocationChange
        mLocationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
        mCriteria = new Criteria();
        mProvider = mLocationManager.getBestProvider(mCriteria, false);
    }

    @Nullable @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_capture, container, false);
        mImgFot = v.findViewById(R.id.img_photo_taked);

        final EditText inTxtTitle = v.findViewById(R.id.input_txt_title_photo);
        final EditText inTxtTag1 = v.findViewById(R.id.edit_text_tag1);
        final EditText inTxtTag2 = v.findViewById(R.id.edit_text_tag2);
        final EditText inTxtTag3 = v.findViewById(R.id.edit_text_tag3);



        btnTakePhoto = v.findViewById(R.id.btn_capture);
        btnTakePhoto.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

                File photoFile = null;
                try {
                    photoFile = createImageFile();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                if (photoFile != null) {
                    mImageUri = Uri.fromFile(photoFile);
                    cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, mImageUri);
                    startActivityForResult(cameraIntent, CAMERA_REQUEST);
                }
            }
        });

        btnSavePhoto = v.findViewById(R.id.btn_save);
        btnSavePhoto.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                if(mImageUri != null ) { //hay fichero
                    if (!mImageUri.equals(Uri.EMPTY)) { //el fichero no esta vacio talvez por que cancelo el usuario
                        Photo photo = new Photo();

                        // OBTENEMOS UBICACION YA ACTUALIZADA EN ON_CHANGE_LOCATION
                        photo.setLocation(mLocation);

                        photo.setTitle(inTxtTitle.getText().toString());
                        photo.setStorageLocation(mImageUri);
                        photo.setTag1(inTxtTag1.getText().toString());
                        photo.setTag2(inTxtTag2.getText().toString());
                        photo.setTag3(inTxtTag3.getText().toString());

                        mDataManager.addPhoto(photo);
                        Toast.makeText(getActivity(), "Se guardo la foto en BD", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(getActivity(), "No ahi imagen a guardar", Toast.LENGTH_LONG).show();
                    }
                } else {
                    Log.e("CaptureFragment", "onClick() returned: " + "Error Tecnico no ahi Uri que guardar para guardar la photo");
                }
            }
        });

        btnCancel = v.findViewById(R.id.btn_cancel);
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                turnOnCapture();
            }
        });
        return v;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if(requestCode == CAMERA_REQUEST && resultCode == Activity.RESULT_OK) {
            try { mImgFot.setImageURI(Uri.parse(mImageUri.toString())); turnOnSave(); }
            catch (Exception e) { e.printStackTrace(); }
        } else {
            mImageUri = null; turnOnCapture(); //LIBERAMOS MEMORIA YA QUE ALGO NO SALIO BIEN
        }
    }

    private File createImageFile() throws IOException {
        //Creamos el nombre de la foto basado en la fecha en que ha sido tomada
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "IMG_"+timeStamp+"_";

        //@deprecated File storeageDirectory = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        File storeageDirectory =  getActivity().getBaseContext().getExternalFilesDir(null);

        File image = File.createTempFile(
                imageFileName, //nombre del fichero a crear
                ".jpg", //extensión del fichero
                storeageDirectory //directorio o carpeta donde guardamos el fichero
        );

        //Guardamos para utilizarlo con el intent de Action View
        mCurrentPhotoPath = "file: "+image.getAbsolutePath();
        return image;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        //BitmapDrawable bd = (BitmapDrawable) mImgFot.getDrawable();
        //bd.getBitmap().recycle();
        //mImgFot.setImageBitmap(null); //ES RECOMENDABE LIMPIAR LAS IMAGENES
    }

    private void turnOnSave() {
        btnSavePhoto.setVisibility(View.VISIBLE);
        btnTakePhoto.setVisibility(View.GONE);
    }

    private void turnOnCapture() {
        btnSavePhoto.setVisibility(View.GONE);
        btnTakePhoto.setVisibility(View.VISIBLE);
    }

    @Override
    public void onLocationChanged(Location location) {
        mLocation = location;
    }

    @Override
    public void onResume() {
        super.onResume();
        boolean fineLocationGranted = ContextCompat.checkSelfPermission( getActivity().getApplicationContext(), android.Manifest.permission.ACCESS_FINE_LOCATION ) != PackageManager.PERMISSION_GRANTED;
        boolean fineLocationCoarse = ContextCompat.checkSelfPermission( getActivity().getApplicationContext(), android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED;
        if ( Build.VERSION.SDK_INT >= 23 && fineLocationGranted && fineLocationCoarse) {
            //PEDIMOS PERMISOS DE ACCESO DE GEOLOCATION
            ActivityCompat.requestPermissions(getActivity(),
                    new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION, android.Manifest.permission.ACCESS_COARSE_LOCATION},
                    GEOLACATION_REQUEST_PERMISION);
        } else {
            mProvider = mLocationManager.getBestProvider(mCriteria, false);
            mLocationManager.requestLocationUpdates(mProvider, 500, 1, this);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        mLocationManager.removeUpdates(this);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case GEOLACATION_REQUEST_PERMISION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! Do the contacts-related task you need to do.
                } else {
                    // permission denied, boo! Disable the functionality that depends on this permission.
                    Toast.makeText(getActivity(), "Las fotos se guardaran sin geo-posicionamiento", Toast.LENGTH_LONG).show();
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request.
        }
    }

    //no los usamos en este caso
    @Override public void onStatusChanged(String provider, int status, Bundle extras) { }
    @Override public void onProviderEnabled(String provider) { }
    @Override public void onProviderDisabled(String provider) { }
}
