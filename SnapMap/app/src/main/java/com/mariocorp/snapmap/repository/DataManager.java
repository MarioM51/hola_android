package com.mariocorp.snapmap.repository;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.mariocorp.snapmap.model.Photo;

/**
 * Created by JuanGabriel on 6/2/18.
 */

public class DataManager {
    public static final String TAG = "DataManager";

    private SQLiteDatabase db;

    public static final String TABLE_ROW_ID = "_id";
    public static final String TABLE_ROW_TITLE = "image_title";
    public static final String TABLE_ROW_URI = "image_uri";

    public static final String TABLE_ROW_TAG = "tag";
    private static final String TABLE_ROW_TAG1 = "tag1";
    private static final String TABLE_ROW_TAG2 = "tag2";
    private static final String TABLE_ROW_TAG3 = "tag3";

    private static final String DB_NAME = "snapmap_db";
    private static final int DB_VERSION = 2;
    private static final String TABLE_PHOTOS = "snapmap_table_photos";
    private static final String TABLE_TAGS = "snapmap_table_tags";

    //campos de la version 2
    public static final String TABLE_ROW_LOCATION_LAT ="gps_location_lat";
    public static final String TABLE_ROW_LOCATION_LONG ="gps_location_long";

    public DataManager(Context context){
        SnapMapSQLiteOpenHelper helper = new SnapMapSQLiteOpenHelper(context);
        db = helper.getWritableDatabase();
    }

    /*METODOS DEL DATA MANAGER*/
    public void addPhoto(Photo photo){

        String query = "INSERT INTO "+
                TABLE_PHOTOS + " ("+ TABLE_ROW_TITLE+ ", "+ TABLE_ROW_URI + ", "+ TABLE_ROW_TAG1 + ", "+ TABLE_ROW_TAG2 + ", "+ TABLE_ROW_TAG3 + ", "+ TABLE_ROW_LOCATION_LAT + ", "+ TABLE_ROW_LOCATION_LONG + ") " +
                "VALUES ("+ "'"+photo.getTitle()+"', "+ "'"+photo.getStorageLocation()+"', "+ "'"+photo.getTag1()+"', "+ "'"+photo.getTag2()+"', "+ "'"+photo.getTag3()+"'" +", "+ ""+photo.getLocation().getLatitude()+"" + ", " + ""+photo.getLocation().getLongitude()+"" + ");";
        Log.i(TAG, query);
        db.execSQL(query);
        addTag(photo.getTag1());
        addTag(photo.getTag2());
        addTag(photo.getTag3());
    }


    private void addTag(String tag){
        String query = "INSERT INTO "+ TABLE_TAGS+ " ("+ TABLE_ROW_TAG + ") "+
                "SELECT '"+tag+"' "+ "WHERE NOT EXISTS ("+ "SELECT 1 FROM "+ TABLE_TAGS+ " WHERE "+TABLE_ROW_TAG+" = '"+tag+"'"+ ");";
        Log.i(TAG, query);
        db.execSQL(query);
    }


    public Cursor getTitles(){
        String query = "SELECT "+ TABLE_ROW_ID+", "+TABLE_ROW_TITLE+ " FROM "+TABLE_PHOTOS;
        Log.i(TAG, query);
        Cursor c = db.rawQuery(query,null);
        c.moveToFirst();
        return c;
    }

    public Cursor getTitlesWithTag(String tag){
        String query = "SELECT "+
                TABLE_ROW_ID+ ", "+TABLE_ROW_TITLE + " FROM "+TABLE_PHOTOS+ " WHERE "+
                TABLE_ROW_TAG1 + " = '"+tag+"' OR "+
                TABLE_ROW_TAG2 + " = '"+tag+"' OR "+
                TABLE_ROW_TAG3 + " = '"+tag+"';";
        Log.i(TAG, query);
        Cursor c = db.rawQuery(query, null);
        c.moveToFirst();
        return c;

    }

    public Cursor getPhoto(int id){
        String query = "SELECT * FROM "+TABLE_PHOTOS+" WHERE "+TABLE_ROW_ID+" = "+id;
        Log.i(TAG, query);
        Cursor c = db.rawQuery(query, null);
        c.moveToFirst();
        return c;
    }


    public Cursor getTags(){
        String query = "SELECT "+TABLE_ROW_ID+ ", "+TABLE_ROW_TAG +" FROM "+TABLE_TAGS;
        Log.i(TAG, query);
        Cursor c = db.rawQuery(query,null);
        c.moveToFirst();
        return c;
    }

    private class SnapMapSQLiteOpenHelper extends SQLiteOpenHelper{
        public SnapMapSQLiteOpenHelper(Context context) {
            super(context, DB_NAME, null, DB_VERSION);
        }

        @Override public void onCreate(SQLiteDatabase db) {
            Log.i(TAG, "##################################");
            String newTableQuery = "CREATE TABLE "+ TABLE_PHOTOS + " ("+
                    TABLE_ROW_ID + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, "+
                    TABLE_ROW_TITLE + " TEXT NOT NULL, "+
                    TABLE_ROW_URI + " TEXT NOT NULL, "+
                    TABLE_ROW_LOCATION_LAT + "REAL, " +     //cambio para la v2
                    TABLE_ROW_LOCATION_LONG + "REAL, " +    //cambio para la v2
                    TABLE_ROW_TAG1 + " TEXT NOT NULL, "+
                    TABLE_ROW_TAG2 + " TEXT NOT NULL, "+
                    TABLE_ROW_TAG3 + " TEXT NOT NULL"+ ");";
            Log.i(TAG, newTableQuery);
            db.execSQL(newTableQuery);

            newTableQuery = "CREATE TABLE "+
                    TABLE_TAGS + " ("+
                    TABLE_ROW_ID + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, "+
                    TABLE_ROW_TAG + " TEXT NOT NULL"+ ");";
            Log.i(TAG, newTableQuery);
            db.execSQL(newTableQuery);
            Log.i(TAG, "##################################");
        }


        @Override public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            if(oldVersion < 2) {
                String addColLong = "ALTER TABLE " + TABLE_PHOTOS + " ADD " + TABLE_ROW_LOCATION_LONG + " REAL;";
                String addColLat = "ALTER TABLE " + TABLE_PHOTOS + " ADD " + TABLE_ROW_LOCATION_LAT + " REAL;";
                db.execSQL(addColLong);
                db.execSQL(addColLat);
            }
        }
    }


}
